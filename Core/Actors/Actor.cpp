#include "Actor.h"

#include "ActorComponent.h"

#include <SDL2/SDL_assert.h>

namespace TempoEngine
{

Actor::Actor(ActorId id)
    : mId(id)
{
}

Actor::~Actor()
{
    SDL_assert(mComponents.empty());
}

void Actor::Destroy()
{
    mComponents.clear();
}

bool Actor::Init(rapidxml::xml_node<>* pData)
{
    //TODO: Implement me
    return true;
}

void Actor::PostInit()
{
    for(auto& component : mComponents) {
        component.second->VPostInit();
    }
}

void Actor::Update(U32 milis)
{
    for(auto& component : mComponents) {
        component.second->VUpdate(milis);
    }
}

void Actor::AddComponent(StrongActorComponentPtr pComp)
{
    auto result = mComponents.insert(std::make_pair(pComp->VGetComponentId(), pComp));
    SDL_assert(result.second);
}

} //end of namespace
