#ifndef ACTOR_H
#define ACTOR_H

#include "../Interfaces.h"
#include "../Resources/XMLResource.h"
#include <map>

namespace TempoEngine
{

class Actor
{
    friend class ActorFactory;

public:
    typedef std::map<ComponentId, StrongActorComponentPtr> ActorComponentMap;

    explicit Actor(ActorId id);
    ~Actor();

    bool Init(rapidxml::xml_node<>* pData);
    void PostInit();
    void Destroy();
    void Update(U32 milis);

    ActorId GetId() const { return mId; }

    template <typename ComponentType>
    inline std::weak_ptr<ComponentType> GetComponent();

private:
    ActorId mId;
    ActorComponentMap mComponents;

    //Only called from ActorFactory
    void AddComponent(StrongActorComponentPtr pComp);
};

template <typename ComponentType>
inline std::weak_ptr<ComponentType> Actor::GetComponent()
{
    auto comp = mComponents.find(ComponentType::sCompId);
    if(comp != mComponents.end()) {
        return std::weak_ptr<ComponentType>(std::static_pointer_cast<ComponentType>(comp->second));
    }
    return std::weak_ptr<ComponentType>();
}

} //end of namespace

#endif // ACTOR_H
