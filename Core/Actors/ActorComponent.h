//
//  ActorComponent.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/19/14.
//
//

#ifndef Engine_ActorComponent_h
#define Engine_ActorComponent_h

#include "../Platform.h"
#include "Actor.h"

namespace TempoEngine
{
class ActorComponent
{
public:
    virtual ~ActorComponent() { mpOwner.reset(); }

    virtual bool VInit(rapidxml::xml_node<>* pData) = 0;
    virtual void VPostInit() {}
    virtual void VUpdate(U32 deltaMs) {}

    virtual ComponentId VGetComponentId() const = 0;

private:
    friend class ActorFactory;

    void SetOwner(StrongActorPtr pOnwer) { mpOwner = pOnwer; }

protected:
    StrongActorPtr mpOwner;
};
} //end of namespace

#endif
