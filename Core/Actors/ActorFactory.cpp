#include "ActorFactory.h"
#include "ActorComponent.h"
#include "../Resources/XMLResource.h"
#include "../game.h"

#include "CommonActorComponents.h"

namespace TempoEngine
{

ActorFactory::ActorFactory()
    : mLastActorId(0)
{
#define REGISTER_COMPONENT(comp) mActorComponentCreators[comp::sCompId] = GenericObjectCreationFunction<ActorComponent, comp>;
    
    REGISTER_COMPONENT(TransformComponent);
    REGISTER_COMPONENT(MeshRenderComponent);
    REGISTER_COMPONENT(LightComponent);
    
#undef REGISTER_COMPONENT
}

StrongActorPtr ActorFactory::CreateActor(const ResourceIdentifier& actorResource)
{
    auto res = gGame->GetResourceManager().GetResource<XMLResource>(actorResource);

    StrongActorPtr pActor = std::make_shared<Actor>(this->GetNextActorId());
    if(!pActor->Init(res->doc.first_node())) {
        LOG(ERROR) << "Failed to initialize actor: " + actorResource;
        return StrongActorPtr();
    }

    for(auto pNode = res->doc.first_node()->first_node(); pNode; pNode = pNode->next_sibling()) {
        StrongActorComponentPtr pComponent(this->VCreateComponent(pNode));
        if(pComponent) {
            pActor->AddComponent(pComponent);
            pComponent->SetOwner(pActor);
        } else {
            LOG(ERROR) << "Failed to create actor component";
            return StrongActorPtr();
        }
    }

    pActor->PostInit(); 

    return pActor;
}

StrongActorComponentPtr ActorFactory::VCreateComponent(rapidxml::xml_node<>* pData)
{
    std::string name(pData->name());
    StrongActorComponentPtr pComponent;

    auto findIt = mActorComponentCreators.find(name);
    if(findIt != mActorComponentCreators.end()) {
        ActorComponentCreator creator = findIt->second;
        pComponent.reset(creator());
    } else {
        LOG(ERROR) << "Coudn't find Actor Component named " + name;
        return StrongActorComponentPtr();
    }

    if(pComponent) {
        if(!pComponent->VInit(pData)) {
            LOG(ERROR) << "Component failed to initialize " + name;
            return StrongActorComponentPtr();
        }
    }

    return pComponent;
}

} //end of namespace
