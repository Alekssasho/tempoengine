#ifndef ACTORFACTORY_H
#define ACTORFACTORY_H

#include "../Platform.h"
#include "Actor.h"

#include <string>
#include <map>
#include <memory>

namespace TempoEngine
{

typedef ActorComponent* (*ActorComponentCreator)();
typedef std::map<std::string, ActorComponentCreator> ActorComponentCreatorMap;

class ActorFactory
{
public:
    ActorFactory();
    virtual ~ActorFactory() {}
    StrongActorPtr CreateActor(const ResourceIdentifier& actorResource);

protected:
    ActorComponentCreatorMap mActorComponentCreators;

    virtual StrongActorComponentPtr VCreateComponent(rapidxml::xml_node<>* pData);

private:
    ActorId mLastActorId;

    ActorId GetNextActorId() { return ++mLastActorId; }
};

} //end of TempoEngine namespace

#endif // ACTORFACTORY_H
