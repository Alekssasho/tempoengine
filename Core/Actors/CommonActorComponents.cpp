//
//  CommonActorComponents.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 11/3/14.
//
//

#include "CommonActorComponents.h"
#include <string>
#include "../EventManager/EventManager.h"
#include "../EventManager/CommonEvents.h"
#include "../Mesh.h"

namespace TempoEngine
{
    const ComponentId TransformComponent::sCompId("TransformComponent");
    const ComponentId MeshRenderComponent::sCompId("MeshRenderComponent");
    const ComponentId LightComponent::sCompId("LightComponent");
    
    bool TransformComponent::VInit(rapidxml::xml_node<>* pData)
    {
        auto pos = pData->first_node("Position");
        if(pos) {
            mTransform.SetPosition(Vector3(std::stof(pos->first_attribute("x")->value()),
                                           std::stof(pos->first_attribute("y")->value()),
                                           std::stof(pos->first_attribute("z")->value())));
        } else {
            LOG(INFO) << "Transform Component without position";
        }
        
        auto ypr = pData->first_node("Rotation");
        if (ypr) {
            mTransform.SetRotation(Vector3(std::stof(ypr->first_attribute("yaw")->value()),
                                           std::stof(ypr->first_attribute("pitch")->value()),
                                           std::stof(ypr->first_attribute("roll")->value())));
        } else {
            LOG(INFO) << "Transform Component without rotations";
        }
        
        auto scale = pData->first_node("Scale");
        if (scale) {
            mTransform.SetScale(Vector3(std::stof(scale->first_attribute("x")->value()),
                                        std::stof(scale->first_attribute("y")->value()),
                                        std::stof(scale->first_attribute("z")->value())));
        } else {
            LOG(INFO) << "Transform Component without scale";
        }
        
        return true;
    }
    
    void BaseRenderComponent::VPostInit()
    {
        const auto& pSceneNode = this->VCreateSceneNode();
        EventManager::Get()->TriggerEvent(std::make_shared<EventNewRenderComponentData>(mpOwner->GetId(), pSceneNode));
    }
    

    bool MeshRenderComponent::VInit(rapidxml::xml_node<>* pData)
    {
        auto xmlMeshName = pData->first_attribute("name");
        if(!xmlMeshName) {
            LOG(ERROR) << "Mesh component without mesh name";
            return false;
        }
        
        mMeshName = xmlMeshName->value();
        
        return true;
    }
    
    Renderer::ISceneNodePtr MeshRenderComponent::VCreateSceneNode()
    {
        auto pTransformComponent = mpOwner->GetComponent<TransformComponent>().lock();
        
        if (!pTransformComponent) {
            LOG(ERROR) << "Mesh component without transform component";
            return Renderer::ISceneNodePtr();
        }
        
        Transform* transform = &pTransformComponent->GetTransform();
        
        return std::make_shared<Renderer::GLShaderMeshNode>(mpOwner->GetId(),
                                                            mMeshName,
                                                            Renderer::RenderPass::Actor,
                                                            transform);
    }
    
    bool LightComponent::VInit(rapidxml::xml_node<>* pData)
    {
        //TODO: implements Point and Spot light xml attribues
        using namespace Renderer;
        LightProperties prop;
        
        if(auto xmlType = pData->first_attribute("type")) {
            const std::string type = xmlType->value();
            if(type == "Dir") {
                prop.type = LightType::Directional;
            } else if(type == "Point") {
                prop.type = LightType::Point;
            } else {
                prop.type = LightType::Spot;
            }
        } else {
            LOG(ERROR) << "Light component without type";
            return false;
        }
        
        if (auto xmlBase = pData->first_node("Base")) {
            if (auto xmlColor = xmlBase->first_node("Color")) {
                prop.color = Vector3(std::stof(xmlColor->first_attribute("r")->value()),
                                     std::stof(xmlColor->first_attribute("g")->value()),
                                     std::stof(xmlColor->first_attribute("b")->value()));
            }
            
            if (auto xmlIntensity = xmlBase->first_node("Intensity")) {
                prop.ambientIntensity = std::stof(xmlIntensity->first_attribute("ambient")->value());
                prop.diffuseIntensity = std::stof(xmlIntensity->first_attribute("diffuse")->value());
            }
        }
        
        if(auto xmlDirection = pData->first_node("Direction")) {
            prop.direction = Vector3(std::stof(xmlDirection->first_attribute("x")->value()),
                                     std::stof(xmlDirection->first_attribute("y")->value()),
                                     std::stof(xmlDirection->first_attribute("z")->value()));
        }
        
        mProps = prop;
        return true;
    }
    
    Renderer::ISceneNodePtr LightComponent::VCreateSceneNode()
    {
        return std::make_shared<Renderer::LightNode>(mpOwner->GetId(),
                                                     "LightComponent",
                                                     mProps,
                                                     &gDefaultTransform);
    }
    
}//end of namespace