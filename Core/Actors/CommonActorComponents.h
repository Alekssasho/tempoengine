//
//  CommonActorComponents.h
//  Engine
//
//  Created by Aleksandar Angelov on 11/3/14.
//
//

#ifndef __Engine__CommonActorComponents__
#define __Engine__CommonActorComponents__

#include "ActorComponent.h"
#include "../Renderer/Lights.h"

namespace TempoEngine
{
    class TransformComponent : public ActorComponent
    {
    public:
        static const ComponentId sCompId;
        bool VInit(rapidxml::xml_node<>* pData) override;
        ComponentId VGetComponentId() const override { return sCompId; }
        
        Transform& GetTransform() { return mTransform; }
    private:
        Transform mTransform;
    };
    
    class BaseRenderComponent : public ActorComponent
    {
    public:
        virtual Renderer::ISceneNodePtr VCreateSceneNode() = 0;
        void VPostInit() override;
    };
    
    class MeshRenderComponent : public BaseRenderComponent
    {
    public:
        static const ComponentId sCompId;
        bool VInit(rapidxml::xml_node<>* pData) override;
        ComponentId VGetComponentId() const override { return sCompId; }
        
        Renderer::ISceneNodePtr VCreateSceneNode() override;
    private:
        std::string mMeshName;
    };
    
    class LightComponent : public BaseRenderComponent
    {
    public:
        static const ComponentId sCompId;
        bool VInit(rapidxml::xml_node<>* pData) override;
        ComponentId VGetComponentId() const override { return sCompId; }
        
        Renderer::ISceneNodePtr VCreateSceneNode() override;
    private:
        Renderer::LightProperties mProps;
    };
}//end of namespace


#endif /* defined(__Engine__CommonActorComponents__) */
