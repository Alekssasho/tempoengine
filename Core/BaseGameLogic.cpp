//
//  BaseGameLogic.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 7/7/14.
//
//

#include "BaseGameLogic.h"
#include "HumanView.h"
#include "game.h"
#include <algorithm>
#include <memory>

namespace TempoEngine
{
BaseGameLogic::BaseGameLogic()
    : mLifetime(0)
    , mState(BaseGameState::Initializing)
{
    //TODO: Register script events
}

BaseGameLogic::~BaseGameLogic()
{
    mGameViews.clear();

    if(mpActorFactory)
        delete mpActorFactory;

    for(const auto& actor : mActors) {
        actor.second->Destroy();
    }

    mActors.clear();
}

bool BaseGameLogic::Init()
{
    mpActorFactory = this->VCreateActorFactory();
    return true;
}

bool BaseGameLogic::VLoadGame(const std::string& levelResource)
{
    //TODO: Implement game loading
    
    
    for(const auto& pGameView : mGameViews) {
        if (pGameView->VGetType() == GameViewType::Human) {
            HumanView* pHumanView = static_cast<HumanView*>(pGameView.get());
            pHumanView->LoadGame();
        }
    }
    return false;
}

StrongActorPtr BaseGameLogic::VCreateActor(const ResourceIdentifier& actorResource, const Matrix4* initialTransform)
{
    StrongActorPtr pActor = mpActorFactory->CreateActor(actorResource);
    if(pActor) {
        mActors.insert(std::make_pair(pActor->GetId(), pActor));
        return pActor;
    } else {
        LOG(ERROR) << "Cound not create actor";
        return StrongActorPtr();
    }
}

void BaseGameLogic::VDestroyActor(const ActorId id)
{
    //TODO: Trigger event for destroyng actor

    auto findIt = mActors.find(id);
    if(findIt != mActors.end()) {
        findIt->second->Destroy();
        mActors.erase(findIt);
    }
}

WeakActorPtr BaseGameLogic::VGetActor(const ActorId id)
{
    auto findIt = mActors.find(id);
    if(findIt != mActors.end())
        return findIt->second;
    return WeakActorPtr();
}

void BaseGameLogic::VOnUpdate(unsigned long deltaMs)
{
    mLifetime += deltaMs;

    switch(mState) {
    case BaseGameState::Initializing:
        this->VChageState(BaseGameState::Loading);
        break;
    case BaseGameState::MainMenu:
        break;
    case BaseGameState::Loading:
        //TODO: Maybe load game ? :D
        break;
    case BaseGameState::Running:
        mProcessManager.UpdateProcesses(deltaMs);
    default:
        break;
    }

    for(const auto& gameView : mGameViews)
        gameView->VOnUpdate(deltaMs);

    for(const auto& actor : mActors)
        actor.second->Update(deltaMs);
}

void BaseGameLogic::VChageState(BaseGameState newState)
{
    if (newState == BaseGameState::Loading) {
        gGame->VLoadGame();
        this->VChageState(BaseGameState::Running);
        return;
    }
    mState = newState;
}

void BaseGameLogic::VAddView(std::unique_ptr<IGameView> pView, ActorId id)
{
    int viewId = static_cast<int>(mGameViews.size());
    mGameViews.push_back(std::move(pView));
    const auto& view = mGameViews.back();
    view->VOnAttach(viewId, id);
    view->VOnCreate();
}

void BaseGameLogic::VRemoveView(GameViewId gonnerId)
{
    auto findIt = std::find_if(mGameViews.begin(), mGameViews.end(), [gonnerId](const std::unique_ptr<IGameView>& pView) {
        return pView->VGetId() == gonnerId;
    });
    if(findIt != mGameViews.end()) {
        mGameViews.erase(findIt);
    }
}

ActorFactory* BaseGameLogic::VCreateActorFactory()
{
    return new ActorFactory;
}

} //end of namespace TempoEngine