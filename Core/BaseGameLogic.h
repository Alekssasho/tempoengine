//
//  BaseGameLogic.h
//  Engine
//
//  Created by Aleksandar Angelov on 7/7/14.
//
//

#ifndef __Engine__BaseGameLogic__
#define __Engine__BaseGameLogic__

#include "Interfaces.h"
#include "ProcessManager.h"
#include "Actors/ActorFactory.h"
#include <map>

namespace TempoEngine
{

enum class BaseGameState {
    Invalid,
    Initializing,
    MainMenu,
    Loading,
    Running
};

using ActorMap = std::map<ActorId, StrongActorPtr>;
using GameViewList = std::vector<std::unique_ptr<IGameView>>;

class BaseGameLogic : public IGameLogic
{
protected:
    friend class Game;

    float mLifetime;
    ProcessManager mProcessManager;

    //TODO: Implment Random generator

    ActorMap mActors;
    BaseGameState mState;

    GameViewList mGameViews;

    ActorFactory* mpActorFactory;

    virtual ActorFactory* VCreateActorFactory();

public:
    BaseGameLogic();
    virtual ~BaseGameLogic() override;
    bool Init();

    virtual void VAddView(std::unique_ptr<IGameView> pView, ActorId actorId = INVALID_ACTOR_ID);
    virtual void VRemoveView(GameViewId gonnerId);

    virtual WeakActorPtr VGetActor(const ActorId id) override;
    virtual StrongActorPtr VCreateActor(const ResourceIdentifier& actorResource, const Matrix4* initialTransform = nullptr) override;
    virtual void VDestroyActor(const ActorId id) override;
    virtual void VMoveActor(const ActorId id, const Matrix4& mat) override {}

    virtual bool VLoadGame(const std::string& levelResource) override;

    virtual void VOnUpdate(unsigned long deltaMs) override;

    virtual void VChageState(BaseGameState newState) override;
    BaseGameState GetState() const { return mState; }

    void AttachProcess(StrongProcessPtr pProcess) { mProcessManager.AttachProcess(pProcess); }
};

} //end of namespace TempoEngine
#endif /* defined(__Engine__BaseGameLogic__) */
