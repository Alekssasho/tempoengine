#ifndef DEFAULTTEXTURED_H
#define DEFAULTTEXTURED_H

#include "../Renderer/shaderprogram.h"

namespace TempoEngine
{
namespace Renderer
{
    class DefaultTextured : public ShaderProgram
    {
    public:
        DefaultTextured()
        {
            GLuint vertexShaderID = this->CompileShader(PathToEffect("defaultVS.glsl"), ShaderType::VERTEX);
            GLuint fragmentShaderID = this->CompileShader(PathToEffect("defaultFS.glsl"), ShaderType::FRAGMENT);

            this->Finalize();

            TEMPO_GL_CHECK_ERROR(glDetachShader(mShaderProgramID, vertexShaderID));
            TEMPO_GL_CHECK_ERROR(glDetachShader(mShaderProgramID, fragmentShaderID));
            TEMPO_GL_CHECK_ERROR(glDeleteShader(vertexShaderID));
            TEMPO_GL_CHECK_ERROR(glDeleteShader(fragmentShaderID));
        }
    };

} //end of namespace Renderer
} //end of namespace TempoEngine
#endif // DEFAULTTEXTURED_H
