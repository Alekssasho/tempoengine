#ifndef LIGHTING_H
#define LIGHTING_H

#include "../Renderer/shaderprogram.h"
#include "../TempoMath.h"
#include <vector>

#include "../Renderer/Scene.h"
#include "../Renderer/Lights.h"

namespace TempoEngine
{
namespace Renderer
{

    class Lighting : public ShaderProgram
    {
    public:
        Lighting()
        {
            GLuint vertexShaderID = this->CompileShader(PathToEffect("lightVS.glsl"), ShaderType::VERTEX);
            GLuint fragmentShaderID = this->CompileShader(PathToEffect("lightFS.glsl"), ShaderType::FRAGMENT);

            this->Finalize();

            TEMPO_GL_CHECK_ERROR(glDetachShader(mShaderProgramID, vertexShaderID));
            TEMPO_GL_CHECK_ERROR(glDetachShader(mShaderProgramID, fragmentShaderID));
            TEMPO_GL_CHECK_ERROR(glDeleteShader(vertexShaderID));
            TEMPO_GL_CHECK_ERROR(glDeleteShader(fragmentShaderID));
        }

        void SetDirectionalLight(const LightProperties& light)
        {
            U32 uniformLocation = this->GetUniformLocation("gDirLight.base.color");
            this->SetBaseLight(light, uniformLocation);
            TEMPO_GL_CHECK_ERROR(glUniform3fv(uniformLocation++, 1, glm::value_ptr(glm::normalize(light.direction))));
        }

        void SetPointLight(U32 numLights, const LightProperties* pLights)
        {
            static const U32 maxLights = 2;
            if(numLights > maxLights) {
                LOG(FATAL) << "This Shader doesn't support " << numLights << " point lights.";
                return;
            }

            TEMPO_GL_CHECK_ERROR(glUniform1i(this->GetUniformLocation("gNumPointLights"), numLights));
            U32 uniformLocation = this->GetUniformLocation("gPointLights[0].base.color");
            for(U32 i = 0; i < numLights; ++i) {
                this->SetPointLight(pLights[i], uniformLocation);
            }
        }

        void SetSpotLights(U32 numLights, const LightProperties* pLights)
        {
            static const U32 maxLights = 2;
            if(numLights > maxLights) {
                LOG(FATAL) << "This Shader doesn't support " << numLights << " spot lights.";
                return;
            }
            TEMPO_GL_CHECK_ERROR(glUniform1i(this->GetUniformLocation("gNumSpotLights"), numLights));
            U32 uniformLocation = this->GetUniformLocation("gSpotLights[0].base.base.color");
            for(U32 i = 0; i < numLights; ++i) {
                this->SetPointLight(pLights[i], uniformLocation);
                TEMPO_GL_CHECK_ERROR(glUniform3fv(uniformLocation++, 1, glm::value_ptr(glm::normalize(pLights[i].direction))));
                TEMPO_GL_CHECK_ERROR(glUniform1f(uniformLocation++, glm::cos(glm::radians(pLights[i].cutoff))));
            }
        }

        void SetEyeWorldPos(const Vector3& eye)
        {
            TEMPO_GL_CHECK_ERROR(glUniform3fv(this->GetUniformLocation("gEyeWorldPos"), 1, glm::value_ptr(eye)));
        }

        void SetMatSpecularIntensity(float intensity)
        {
            TEMPO_GL_CHECK_ERROR(glUniform1f(this->GetUniformLocation("gMatSpecularIntensity"), intensity));
        }

        void SetMatSpecularPower(float power)
        {
            TEMPO_GL_CHECK_ERROR(glUniform1f(this->GetUniformLocation("gSpecularPower"), power));
        }

        bool SetupRender(Scene* pScene, SceneNode* pNode) override
        {
            this->ActiveProgram();

            const Matrix4& MVP = pScene->GetCamera()->GetWorldViewProjection(pScene);
            const Matrix4& worldMatrix = pScene->GetTopMatrix();

            this->SetMatrix("MVP", MVP);
            this->SetMatrix("gWorld", worldMatrix);
            
            Color unused;
            float specPower;
            pNode->VGet()->GetMaterial().GetSpecular(unused, specPower);
            this->SetMatSpecularPower(specPower);

            LightingBuffer buffer;
            pScene->GetLightManager()->CalcLighting(buffer, pNode);
//            LOG(INFO) << glm::to_string(buffer.dirLight.color) << "\n" << glm::to_string(buffer.dirLight.direction);

            this->SetPointLight(buffer.numPointLights, buffer.pointLights);
            this->SetSpotLights(buffer.numSpotLights, buffer.spotLights);
            if(buffer.isDirLightAvailable) {
                this->SetDirectionalLight(buffer.dirLight);
            }

            return true;
        }

    private:
        void SetBaseLight(const LightProperties& light, U32& location)
        {
            TEMPO_GL_CHECK_ERROR(glUniform3fv(location++, 1, glm::value_ptr(light.color)));
            TEMPO_GL_CHECK_ERROR(glUniform1f(location++, light.ambientIntensity));
            TEMPO_GL_CHECK_ERROR(glUniform1f(location++, light.diffuseIntensity));
        }

        void SetPointLight(const LightProperties& light, U32& location)
        {
            this->SetBaseLight(light, location);
            //Position
            TEMPO_GL_CHECK_ERROR(glUniform3fv(location++, 1, glm::value_ptr(light.position)));
            //Atten
            TEMPO_GL_CHECK_ERROR(glUniform1f(location++, light.attenuation.constant));
            TEMPO_GL_CHECK_ERROR(glUniform1f(location++, light.attenuation.linear));
            TEMPO_GL_CHECK_ERROR(glUniform1f(location++, light.attenuation.exp));
        }
    };
} //end of namespace Effects
} //end of namespace TempoEngine

#endif // LIGHTING_H
