//
//  CommonEvents.h
//  Engine
//
//  Created by Aleksandar Angelov on 1/28/15.
//
//

#ifndef __Engine__CommonEvents__
#define __Engine__CommonEvents__

#include "EventManager.h"
#include "../Interfaces.h"

namespace TempoEngine
{
    
class EventNewRenderComponentData : public BaseEventData
{
private:
    ActorId mActorId;
    std::shared_ptr<Renderer::ISceneNode> mSceneNode;
public:
    static const EventType kEventType;
    
    EventNewRenderComponentData(ActorId id, const std::shared_ptr<Renderer::ISceneNode>& node)
    : mActorId(id), mSceneNode(node)
    {}
    
    const EventType& VGetEventType() const override { return kEventType; }
    const std::string VGetName() const override { return "Event New Render Component"; }
    
    ActorId GetActorId() const { return mActorId; }
    std::shared_ptr<Renderer::ISceneNode> GetSceneNode() const { return mSceneNode; }
};

}//end of namespace TempoEngine

#endif /* defined(__Engine__CommonEvents__) */
