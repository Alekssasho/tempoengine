//
//  EventManager.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 5/22/14.
//
//

#include "EventManager.h"
#include "../platform.h"
#include <SDL2/SDL.h>

#include <functional>

namespace TempoEngine
{

static EventManager* g_pEventMgr = nullptr;

EventManager* EventManager::Get()
{
    SDL_assert(g_pEventMgr);
    return g_pEventMgr;
}

EventManager::EventManager(const std::string& name, bool setAsGlobal)
:mActiveQueue(0)
{
    if(setAsGlobal) {
        if(g_pEventMgr) {
            LOG(ERROR) << "Attemp to create 2 global IEventManagers, deleting old one and replacing it with new one";
            delete g_pEventMgr;
        }

        g_pEventMgr = this;
    }
}

EventManager::~EventManager()
{
    if(g_pEventMgr == this) {
        g_pEventMgr = nullptr;
    }
}

bool EventManager::AddListener(const EventListenerDelegate& eventDelegate, const EventType& eventType)
{
    VLOG(1) << "Events: Attempt for adding delegate function for type " << eventType;

    //this will find or create it if not exisiting
    EventListenerList& eventListenerList = mEventListeners[eventType];

    auto findIt = std::find(eventListenerList.begin(), eventListenerList.end(), eventDelegate);

    if(findIt != eventListenerList.end()) {
        LOG(ERROR) << "Events: Attempt for adding already existing delegate for event type " << eventType;
        return false;
    }

    eventListenerList.push_back(eventDelegate);
    VLOG(1) << "Events: Succesfully added delegate for event type " << eventType;

    return true;
}

bool EventManager::RemoveListener(const EventListenerDelegate& eventDelegate, const EventType& eventType)
{
    VLOG(1) << "Events: Attempt for removing delegate for type " << eventType;
    bool success = false;

    auto findListIt = mEventListeners.find(eventType);
    if(findListIt != mEventListeners.end()) {
        EventListenerList& eventListenerList = findListIt->second;

        auto findListenerIt = std::find(eventListenerList.begin(), eventListenerList.end(), eventDelegate);
        if(findListenerIt != eventListenerList.end()) {
            eventListenerList.erase(findListenerIt);
            success = true;
            VLOG(1) << "Events: Succesfully deleted event delegate for type " << eventType;
        }
    }

    return success;
}

bool EventManager::TriggerEvent(const IEventDataPtr& pEvent) const
{
    VLOG(1) << "Events: Attempt at triggering event " << pEvent->VGetName();
    bool proccessed = false;

    auto findIt = mEventListeners.find(pEvent->VGetEventType());
    if(findIt != mEventListeners.end()) {
        const EventListenerList& eventListenerList = findIt->second;
        for(const EventListenerDelegate& listener : eventListenerList) {
            VLOG(1) << "Events: Sending Event " << pEvent->VGetName() << " to delegate";
            listener(pEvent);
            proccessed = true;
        }
    }

    return proccessed;
}

bool EventManager::QueueEvent(const IEventDataPtr& pEvent)
{
    SDL_assert(mActiveQueue >= 0);
    SDL_assert(mActiveQueue < EVENTMANAGER_NUM_QUEUES);

    if(!pEvent) {
        LOG(ERROR) << "Invalid Event in QueueEvent()";
        return false;
    }

    VLOG(1) << " Events: Attempting to queue event " << pEvent->VGetName();

    auto findIt = mEventListeners.find(pEvent->VGetEventType());
    if(findIt != mEventListeners.end()) {
        mQueues[mActiveQueue].push_back(pEvent);
        VLOG(1) << "Events: Successfully added event " << pEvent->VGetName();
        return true;
    } else {
        LOG(WARNING) << "Events: Skipping event since there is no listener registered for this event " << pEvent->VGetName();
        return false;
    }
}

bool EventManager::AbortEvent(const EventType& type, bool allOfType)
{
    SDL_assert(mActiveQueue >= 0);
    SDL_assert(mActiveQueue < EVENTMANAGER_NUM_QUEUES);

    bool success = false;
    VLOG(1) << "Events: Attemp at removing event with type" << type;

    auto findIt = mEventListeners.find(type);
    if(findIt != mEventListeners.end()) {
        EventQueue& eventQueue = mQueues[mActiveQueue];

        const auto predicate = [&type](const IEventDataPtr& value) {
                return (value->VGetEventType() == type);
        };

        if(allOfType) {
            eventQueue.remove_if(predicate); //remove all events with same type
            success = true;
            VLOG(1) << "Events: Removed all events with type " << type;
        } else {
            auto findEventIt = std::find_if(eventQueue.begin(), eventQueue.end(), predicate); //find the first event with same type
            if(findEventIt != eventQueue.end()) {
                eventQueue.erase(findEventIt);
                success = true;
                VLOG(1) << "Events: Removed first event with type " << type;
            }
        }
    }

    return success;
}

bool EventManager::Update(unsigned long maxMilis)
{
    unsigned long currMs = SDL_GetTicks();
    unsigned long maxMs = ((maxMilis == kInfinite) ? kInfinite : (currMs + maxMilis));

    int queueToProccess = mActiveQueue;
    mActiveQueue = (mActiveQueue + 1) % EVENTMANAGER_NUM_QUEUES;
    mQueues[mActiveQueue].clear();

    VLOG(1) << "EventLoop: Proccesing Event Queue " << queueToProccess << ";" << mQueues[queueToProccess].size() << " events to Proccess";

    while(!mQueues[queueToProccess].empty()) {
        IEventDataPtr pEvent = mQueues[queueToProccess].front();
        mQueues[queueToProccess].pop_front();
        VLOG(1) << "EventLoop: \t Proccessing Event " << pEvent->VGetName();

        const EventType& type = pEvent->VGetEventType();
        auto findIt = mEventListeners.find(type);
        if(findIt != mEventListeners.end()) {
            const EventListenerList& eventListeners = findIt->second;
            VLOG(1) << "EventLoop: \t Found " << eventListeners.size() << " delegates";

            for(const EventListenerDelegate& listener : eventListeners) {
                listener(pEvent);
            }
        }

        currMs = SDL_GetTicks();
        if(maxMilis != kInfinite && currMs >= maxMs) {
            LOG(INFO) << "EventLoop: Breking event proccessing; time ran out";
            break;
        }
    }

    bool queueFlushed = (mQueues[queueToProccess].empty());
    if(!queueFlushed) {
        //transfer elements to activeQueue
        mQueues[mActiveQueue].splice(mQueues[mActiveQueue].begin(), mQueues[queueToProccess]);
    }

    return queueFlushed;
}

} //end of namespace