//
//  EventManager.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/22/14.
//
//

#ifndef __Engine__EventManager__
#define __Engine__EventManager__

#include <memory>
#include <list>
#include <map>
#include <FastDelegate/FastDelegate.h>

namespace TempoEngine
{

class IEventData;
typedef unsigned long EventType;
typedef std::shared_ptr<IEventData> IEventDataPtr;
typedef fastdelegate::FastDelegate1<IEventDataPtr> EventListenerDelegate;

class IEventData
{
public:
    virtual ~IEventData() {}
    virtual const EventType& VGetEventType() const = 0;
    virtual float VGetTimeStamp() const = 0;
    virtual void VSerialize(std::ostream& out) const = 0;
    virtual void VDeserialize(std::istream& in) = 0;
    //maybe needed later
    //        virtual IEventDataPtr VCopy() const = 0;
    virtual const std::string VGetName() const = 0;
};

class BaseEventData : public IEventData
{
public:
    explicit BaseEventData(const float timeStamp = 0.0f)
        : mTimeStamp(timeStamp)
    {
    }

    virtual ~BaseEventData() {}

    virtual float VGetTimeStamp() const override { return mTimeStamp; }

    virtual void VSerialize(std::ostream& out) const override {}
    virtual void VDeserialize(std::istream& in) override {}

private:
    const float mTimeStamp;
};

class EventManager
{
public:
    static const unsigned int EVENTMANAGER_NUM_QUEUES = 2;
    static const unsigned int kInfinite = 0xffffffff;

    explicit EventManager(const std::string& name, bool setAsGlobal);
    ~EventManager();

    bool AddListener(const EventListenerDelegate& eventDelegate, const EventType& eventType);
    bool RemoveListener(const EventListenerDelegate& eventDelegate, const EventType& eventType);

    bool TriggerEvent(const IEventDataPtr& pEvent) const;
    bool QueueEvent(const IEventDataPtr& pEvent);

    bool AbortEvent(const EventType& type, bool allOfType = false);

    bool Update(unsigned long maxMillis = kInfinite);

    static EventManager* Get();

private:
    typedef std::list<EventListenerDelegate> EventListenerList;
    typedef std::map<EventType, EventListenerList> EventListenerMap;
    typedef std::list<IEventDataPtr> EventQueue;

    EventListenerMap mEventListeners;
    EventQueue mQueues[EVENTMANAGER_NUM_QUEUES];
    int mActiveQueue;
};

} //end of namespace TempoEngine

#endif /* defined(__Engine__EventManager__) */
