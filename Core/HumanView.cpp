//
//  HumanView.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 7/4/14.
//
//

#include "HumanView.h"
#include "Renderer/Scene.h"
#include "game.h"
#include <SDL2/SDL.h>
#include <algorithm>
#include <boost/range/adaptor/reversed.hpp>

namespace TempoEngine
{
constexpr GameViewId INVALID_GAME_VIEW_ID = 0xffffffff;

HumanView::HumanView(std::shared_ptr<Renderer::IRenderer> pRenderer)
{
    InitAudio();

    //Assume user is using mouse, here we should detect tablet uses or use appropriate radius
    mPointerRadius = 1;

    mViewId = INVALID_GAME_VIEW_ID;

    //TODO: Register delegates

    if(pRenderer) {
        using namespace Renderer;
        mpScene = std::make_shared<ScreenElementScene>(pRenderer);
        this->VPushElement(std::static_pointer_cast<IScreenElement>(mpScene));

        
        //TODO: read frustum settings from option file
        Frustum frustum;
        auto screenSize = gGame->GetScreenSize();
        frustum.Init(Degree(60), static_cast<float>(screenSize.x) / screenSize.y, 1.0f, 100.0f);
        std::shared_ptr<CameraNode> pCamera = std::make_shared<CameraNode>(Vector3(0.0f), frustum);
        mpScene->VAddChild(INVALID_ACTOR_ID, pCamera);
        mpScene->GetScene().SetCamera(pCamera);
    }
}

HumanView::~HumanView()
{
    //TODO: Unregister delegates
}
    
bool HumanView::VLoadGameDelegate(/*XML resource*/)
{
//    this->VPushElement(std::static_pointer_cast<IScreenElement>(mpScene));
    return true;

}

bool HumanView::LoadGame()
{
    return this->VLoadGameDelegate();
}

void HumanView::VOnRender()
{
    mCurrTick = SDL_GetTicks();
    if(mCurrTick == mLastDraw)
        return;

    constexpr static float SCREEN_REFRESH_RATE(1000 / 60); // 60 fps

    if(mRunFullSpeed || (mCurrTick - mLastDraw) > SCREEN_REFRESH_RATE) {
        
        if(gGame->GetRenderer()->VPreRender()) {
            std::sort(mScreenElements.begin(), mScreenElements.end(), [](const ScreenElementPtr& lhs, const ScreenElementPtr& rhs) {
                    return (*lhs) < (*rhs);
            });
            for(const auto& pElement : mScreenElements) {
                if(pElement->VIsVisible()) {
                    pElement->VOnRender();
                }
            }

            this->VRenderText();
            mLastDraw = mCurrTick;
            gGame->GetRenderer()->VPostRender();
        }
    }
}

bool HumanView::VOnCreate()
{
    bool result = true;
    for(const auto& pElem : mScreenElements) {
        result &= pElem->VOnCreate();
    }
    return result;
}
    
bool HumanView::VOnMessage(InputEvent event)
{
    for(const auto& screenElement : boost::adaptors::reverse(mScreenElements)) {
        if(screenElement->VOnMessage(event))
            return true;
    }
    
    switch(event.type) {
        case InputEventType::KEYBOARD_DOWN_EVENT:
            return mKeyboardHandler->VOnKeyDown(event.key.code);
        case InputEventType::KEYBOARD_UP_EVENT:
            return mKeyboardHandler->VOnKeyUp(event.key.code);
        default:
            return false;
    }
}

bool HumanView::InitAudio()
{
    //TODO: init audio system here
    return false;
}

void HumanView::TogglePause(bool active)
{
    //TODO: pause and resum audio
}

void HumanView::VOnUpdate(unsigned long deltaMs)
{
    mProcessManager.UpdateProcesses(deltaMs);
    for(const auto& pElem : mScreenElements) {
        pElem->VOnUpdate(deltaMs);
    }
}

void HumanView::VPushElement(ScreenElementPtr pElement)
{
    mScreenElements.push_back(pElement);
}

void HumanView::VRemoveElement(ScreenElementPtr pElement)
{
    using namespace std;
    mScreenElements.erase(remove(mScreenElements.begin(), mScreenElements.end(), pElement), mScreenElements.end());
}

void HumanView::VSetCameraOffset(const Vector4& camOffset)
{
    const auto& camera = mpScene->GetScene().GetCamera();
    if(camera) {
        camera->SetCameraOffset(Vector3(camOffset));
    }
}

} //end of namespace TempoEngine