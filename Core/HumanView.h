//
//  HumanView.h
//  Engine
//
//  Created by Aleksandar Angelov on 7/4/14.
//
//

#ifndef __Engine__HumanView__
#define __Engine__HumanView__

#include "Interfaces.h"
#include "ProcessManager.h"

namespace TempoEngine
{

using ScreenElementPtr = std::shared_ptr<IScreenElement>;

//Forward Declaration
namespace Renderer {
    class ScreenElementScene;
}

class HumanView : public IGameView
{
protected:
    GameViewId mViewId;
    ActorId mActorId;

    ProcessManager mProcessManager;

    unsigned long mCurrTick;
    unsigned long mLastDraw;
    bool mRunFullSpeed;

    virtual void VRenderText() {}

    virtual bool VLoadGameDelegate(/*XML resource*/);

public:
    bool LoadGame(/*XML resource*/);

    bool VOnCreate() override;
    void VOnRender() override;
    GameViewType VGetType() override { return GameViewType::Human; }
    GameViewId VGetId() override { return mViewId; }
    bool VOnMessage(InputEvent event) override;

    void VOnAttach(GameViewId vId, ActorId aId) override
    {
        mViewId = vId;
        mActorId = aId;
    }
    
    void VOnUpdate(unsigned long deltaMs) override;

    virtual void VPushElement(ScreenElementPtr pElement);
    virtual void VRemoveElement(ScreenElementPtr pElement);

    void TogglePause(bool active);

    ~HumanView() override;

    HumanView(std::shared_ptr<Renderer::IRenderer> pRenderer);

    std::vector<ScreenElementPtr> mScreenElements;

    int mPointerRadius;
    std::shared_ptr<IPointerHandler> mPointerHandler;
    std::shared_ptr<IKeyboardHandler> mKeyboardHandler;

    std::shared_ptr<Renderer::ScreenElementScene> mpScene;

    bool InitAudio();

    virtual void VSetCameraOffset(const Vector4& camOffset);
};

} //end of namespace TempoEngine

#endif /* defined(__Engine__HumanView__) */
