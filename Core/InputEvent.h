#ifndef INPUT_EVENT_H
#define INPUT_EVENT_H

#include <SDL2/SDL_keycode.h>

namespace TempoEngine
{
enum class InputEventType {
    KEYBOARD_UP_EVENT,
    KEYBOARD_DOWN_EVENT,
    MOUSE_MOTION_EVENT
};

typedef SDL_Keycode Keycode;

struct KeyboardEvent
{
    InputEventType type;
    Keycode code;
};

struct MouseMotionEvent
{
    InputEventType type;
    int x;
    int y;
};

union InputEvent
{
    InputEventType type;
    KeyboardEvent key;
    MouseMotionEvent motion;
};

} //end of namespace
#endif