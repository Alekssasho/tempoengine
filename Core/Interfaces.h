//
//  Interfaces.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/29/14.
//
//

#ifndef Engine_Interfaces_h
#define Engine_Interfaces_h

#include "Platform.h"
#include "InputEvent.h"
#include <memory>
#include <vector>

namespace TempoEngine
{
class Actor;
class ActorComponent;

using ActorId = U32;
using ComponentId = std::string;

using StrongActorPtr          = std::shared_ptr<Actor>;
using WeakActorPtr            = std::weak_ptr<Actor>;
using StrongActorComponentPtr = std::shared_ptr<ActorComponent>;
using WeakActorComponentPtr   = std::weak_ptr<ActorComponent>;

constexpr ActorId INVALID_ACTOR_ID = 0xffffffff;
const ComponentId INVALID_COMPONENT_ID = "";

//Resource stuff
class Resource;

using ResourceIdentifier = std::string;
using ResourcePtr = std::shared_ptr<Resource>;

class IResourceFile
{
public:
    virtual bool VOpen() = 0;
    virtual int VGetRawResourceSize(const ResourceIdentifier& r) = 0;
    virtual int VGetRawResource(const ResourceIdentifier& r, ByteBuffer& buffer) = 0;
    virtual int VGetNumResources() const = 0;
    virtual ~IResourceFile() {}
};

class IResourceLoader
{
public:
    virtual std::string VGetResType() = 0;
    virtual ResourcePtr VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize) = 0;
    virtual ~IResourceLoader() {}
};

//Input Handlers

class IKeyboardHandler
{
public:
    virtual bool VOnKeyDown(const Keycode c) = 0;
    virtual bool VOnKeyUp(const Keycode c) = 0;
    virtual ~IKeyboardHandler() {}
};

class IPointerHandler
{
public:
    virtual bool VOnPointerMove(const Vector2& pos, const int radius) = 0;
    virtual bool VOnPointerButtonDown(const Vector2& pos, const int radius, const std::string& buttonName) = 0;
    virtual bool VOnPointerButtonUp(const Vector2& pos, const int radius, const std::string& buttonName) = 0;
    virtual ~IPointerHandler() {}
};

//GameView and GameLogic interfaces

enum class BaseGameState;

class IGameLogic
{
public:
    virtual WeakActorPtr VGetActor(const ActorId id) = 0;
    virtual StrongActorPtr VCreateActor(const ResourceIdentifier& actorResource, const Matrix4* initialTransform = nullptr) = 0;
    virtual void VDestroyActor(const ActorId id) = 0;
    virtual bool VLoadGame(const std::string& levelResource) = 0;
    virtual void VOnUpdate(unsigned long deltaTime) = 0;
    virtual void VChageState(BaseGameState newState) = 0;
    virtual void VMoveActor(const ActorId id, const Matrix4& mat) = 0;

    virtual ~IGameLogic() {}
};

class IScreenElement
{
public:
    virtual bool VOnCreate() = 0;
    virtual bool VOnRender() = 0;
    virtual void VOnUpdate(unsigned long deltaMS) = 0;
    virtual int VGetZOrder() const = 0;
    virtual void VSetZOrder(int zOrder) = 0;
    virtual bool VIsVisible() const = 0;
    virtual void VSetVisible(bool visible) = 0;
    virtual bool VOnMessage(InputEvent event) = 0;

    virtual ~IScreenElement() {}
};

inline bool operator<(const IScreenElement& lhs, const IScreenElement& rhs) { return lhs.VGetZOrder() < rhs.VGetZOrder(); }

enum class GameViewType {
    Human,
    Remote,
    AI,
    Recorder,
    Other
};

using GameViewId = U32;

class IGameView
{
public:
    virtual bool VOnCreate() = 0;
    virtual void VOnRender() = 0;
    virtual GameViewType VGetType() = 0;
    virtual GameViewId VGetId() = 0;
    virtual void VOnAttach(GameViewId vId, ActorId aId) = 0;

    virtual void VOnUpdate(unsigned long deltaMs) = 0;
    
    virtual bool VOnMessage(InputEvent event) = 0;

    virtual ~IGameView() {}
};
    
class IScriptManager
{
public:
    virtual ~IScriptManager() {}
    virtual bool VInit() = 0;
    virtual bool VExecuteFile(const ResourceIdentifier& resource) = 0;
    virtual void VExecuteString(const char * str) = 0;
};

namespace Renderer
{
    class SceneNodeProperties;
    class Scene;

    enum class RenderPass {
        Static,
        Actor,
        Sky,
        NotRendered
    };

    class ISceneNode
    {
    public:
        virtual const SceneNodeProperties* const VGet() const = 0;
        virtual bool VOnUpdate(Scene* pScene, const U32 elapsedMs) = 0;
        virtual bool VOnCreate(Scene* pScene) = 0;
        virtual bool VPreRender(Scene* pScene) = 0;
        virtual bool VIsVisible(Scene* pScene) const = 0;
        virtual bool VRender(Scene* pScene) = 0;
        virtual bool VRenderChildren(Scene* pScene) = 0;
        virtual bool VPostRender(Scene* pScene) = 0;
        virtual bool VAddChild(std::shared_ptr<ISceneNode> kid) = 0;
        virtual bool VRemoveChild(ActorId id) = 0;

        //TODO: Replace this with better version, maybe GetType so that every node can be destinguished when needed without dynamic_cast
        virtual bool VIsLight() const = 0;

        virtual ~ISceneNode() {}
    };
    
    using ISceneNodePtr = std::shared_ptr<ISceneNode>;

    class LightNode;

    using LightPtrVector = std::vector<std::shared_ptr<LightNode>>;

    class IRenderState
    {
    public:
        virtual std::string VToString() = 0;
        virtual ~IRenderState() {}
    };

    class IRenderer
    {
    public:
        virtual void VSetBackgroundColor(const Color& color) = 0;
        virtual bool VOnCreate() = 0;
        virtual void VShutdown() = 0;
        virtual bool VPreRender() = 0;
        virtual bool VPostRender() = 0;
        virtual std::shared_ptr<IRenderState> VPrepareAlphaPass() = 0;
        virtual std::shared_ptr<IRenderState> VPrepareSkyBoxPass() = 0;
        virtual void VDrawLine(const Vector3& from, const Vector3& to, const Color& color) = 0;

        virtual ~IRenderer() {}
    };

} //end of namespace Renderer
} //end of namespace TempoEngine

#endif
