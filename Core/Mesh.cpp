#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Mesh.h"
#include "game.h"

#include "ResourceLoading.h"

namespace TempoEngine
{
namespace Renderer
{

    const std::string Mesh::resType = "Mesh";

    Mesh::~Mesh()
    {
        this->Clear();
    }

    void Mesh::Clear()
    {
        mTextures.clear();
    }

    void Mesh::Render()
    {
        for(MeshEntry& mesh : mEntries) {
            mesh.mVArray->Bind();

            const U32 matIndex = mesh.mMaterialIndex;
            if(matIndex < mTextures.size() && mTextures[matIndex]) {
                mTextures[matIndex]->Bind(GL_TEXTURE0);
            }

            glDrawElements(GL_TRIANGLES, mesh.mNumIndices, GL_UNSIGNED_INT, 0);

            mesh.mVArray->Unbind();
        }
    }

    Mesh::MeshEntry::MeshEntry()
        : mVArray(nullptr)
        , mNumIndices(0)
        , mMaterialIndex(INVALID_GRAPHICS_ID)
    {
    }

    void Mesh::MeshEntry::Init(const std::vector<Vertex>& vertices, const std::vector<U32>& indices)
    {
        mNumIndices = indices.size();

        auto& buffManager = HardwareBufferManager::Instance();
        mVArray = buffManager.CreateVertexArray();

        buffManager.CreateVertexBufferForVArray(mVArray,
                                                sizeof(Vertex),
                                                vertices.size(),
                                                BufferUsage::STATIC,
                                                &vertices[0]);

        buffManager.CreateIndexBufferForVArray(mVArray,
                                               sizeof(U32),
                                               mNumIndices,
                                               BufferUsage::STATIC,
                                               &indices[0]);

        buffManager.SetVertexAttributeForVArray(mVArray,
                                                0,
                                                3,
                                                GL_FLOAT,
                                                GL_FALSE,
                                                sizeof(Vertex),
                                                (void*)0);

        buffManager.SetVertexAttributeForVArray(mVArray,
                                                1,
                                                2,
                                                GL_FLOAT,
                                                GL_FALSE,
                                                sizeof(Vertex),
                                                (void*)12);

        buffManager.SetVertexAttributeForVArray(mVArray,
                                                2,
                                                3,
                                                GL_FLOAT,
                                                GL_FALSE,
                                                sizeof(Vertex),
                                                (void*)20);
    }

    ResourcePtr MeshLoader::VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize)
    {
        Assimp::Importer importer;

        const ::aiScene* pScene = importer.ReadFileFromMemory(rawBuffer.get(), rawSize,
                                                              aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

        if(pScene) {
            std::shared_ptr<Mesh> pMesh = std::make_shared<Mesh>();
            pMesh->mEntries.resize(pScene->mNumMeshes);
            pMesh->mTextures.resize(pScene->mNumMaterials);

            for(U32 i = 0; i < pScene->mNumMeshes; ++i) {
                const ::aiMesh* paiMesh = pScene->mMeshes[i];
                this->LoadMesh(pMesh->mEntries[i], paiMesh);
            }

            this->LoadMaterials(pMesh, pScene);

            return pMesh;
        } else {
            LOG(ERROR) << "Error parsing mesh with error: " << importer.GetErrorString();
            return ResourcePtr();
        }
    }

    void MeshLoader::LoadMesh(Mesh::MeshEntry& meshEntry, const aiMesh* pMesh)
    {
        meshEntry.mMaterialIndex = pMesh->mMaterialIndex;
        std::vector<Vertex> vertices;
        std::vector<U32> indices;

        vertices.reserve(pMesh->mNumVertices);
        indices.reserve(pMesh->mNumFaces * 3);

        const aiVector3D zeroVec(0.0f, 0.0f, 0.0f);

        for(U32 i = 0; i < pMesh->mNumVertices; ++i) {
            const aiVector3D* pPos = &(pMesh->mVertices[i]);
            const aiVector3D* pNormal = &(pMesh->mNormals[i]);
            const aiVector3D* pTexCoord = pMesh->HasTextureCoords(0) ? &(pMesh->mTextureCoords[0][i]) : &zeroVec;

            Vertex v(Vector3(pPos->x, pPos->y, pPos->z),
                     Vector2(pTexCoord->x, pTexCoord->y),
                     Vector3(pNormal->x, pNormal->y, pNormal->z));

            vertices.push_back(v);
        }

        for(U32 i = 0; i < pMesh->mNumFaces; ++i) {
            const aiFace& face = pMesh->mFaces[i];
            SDL_assert(face.mNumIndices == 3);
            indices.push_back(face.mIndices[0]);
            indices.push_back(face.mIndices[1]);
            indices.push_back(face.mIndices[2]);
        }

        meshEntry.Init(vertices, indices);
    }

    void MeshLoader::LoadMaterials(std::shared_ptr<Mesh> pMesh, const aiScene* pScene)
    {
        for(U32 i = 0; i < pScene->mNumMaterials; ++i) {
            const aiMaterial* pMaterial = pScene->mMaterials[i];

            if(pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
                aiString path;

                if(pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &path) == AI_SUCCESS) {
                    pMesh->mTextures[i] = gGame->GetResourceManager().GetResource<Texture2D>(path.C_Str());
                }
            }

            if(!pMesh->mTextures[i]) {
                pMesh->mTextures[i] = gGame->GetResourceManager().GetResource<Texture2D>("white.png");
            }
        }
    }

    bool GLShaderMeshNode::VOnCreate(Scene* pScene)
    {
        mpMesh = gGame->GetResourceManager().GetResource<Mesh>(mMeshName);

        return true;
    }

    bool GLShaderMeshNode::VRender(Scene* pScene)
    {
        //Setup Shaders
        mShader.SetupRender(pScene, this);

        mShader.SetSampler("mTextureSampler", 0);
        mpMesh->Render();
        return true;
    }

} //end of namespace Renderer
} //end of namespace TempoEngine
