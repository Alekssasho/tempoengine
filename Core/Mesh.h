#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>
#include <memory>
#include "Platform.h"
#include "TempoMath.h"
#include "Renderer/texture.h"
#include "Renderer/HardwareBufferManager.h"
#include "Renderer/SceneNodes.h"
#include "Effects/Lighting.h"

//forward declaration

struct aiScene;
struct aiMesh;

namespace TempoEngine
{
namespace Renderer
{
    struct Vertex
    {
        Vector3 mPos;
        Vector2 mTex;
        Vector3 mNormal;

        Vertex()
            : mPos(0.0f, 0.0f, 0.0f)
            , mTex(0.0f, 0.0f)
            , mNormal(0.0f, 0.0f, 0.0f)
        {
        }

        Vertex(const Vector3& pos, const Vector2& tex, const Vector3& norm)
            : mPos(pos)
            , mTex(tex)
            , mNormal(norm)
        {
        }
    };

    class Mesh : public Resource
    {
    public:
        Mesh() = default;
        ~Mesh() override;

        void Render();

        static const std::string resType;

    private:
        void Clear();

        struct MeshEntry
        {
            MeshEntry();

            void Init(const std::vector<Vertex>& vertices, const std::vector<U32>& indices);

            VertexArrayPtr mVArray;
            U32 mNumIndices;
            U32 mMaterialIndex;
        };

        typedef std::shared_ptr<Texture2D> TexturePtr;

        std::vector<MeshEntry> mEntries;
        std::vector<TexturePtr> mTextures;

        friend class MeshLoader;
    };

    class MeshLoader : public IResourceLoader
    {
    public:
        virtual std::string VGetResType() override { return Mesh::resType; };
        virtual ResourcePtr VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize) override;

    private:
        void LoadMesh(Mesh::MeshEntry& entry, const aiMesh* pMesh);
        void LoadMaterials(std::shared_ptr<Mesh> pMesh, const aiScene* pScene);
    };

    using MeshPtr = std::shared_ptr<Mesh>;

    class GLShaderMeshNode : public SceneNode
    {
    public:
        GLShaderMeshNode(const ActorId id,
                         std::string meshName,
                         RenderPass pass,
                         Transform* pTransf)
            : SceneNode(id, "MeshNode", pass, pTransf)
            , mMeshName(meshName)
        {
        }

        virtual bool VOnCreate(Scene* pScene) override;
        virtual bool VRender(Scene* pScene) override;

    protected:
        MeshPtr mpMesh;
        std::string mMeshName;
        Lighting mShader;
    };

} //end of namespace Renderer
} //end of namespace TempoEngine

#endif // MESH_H
