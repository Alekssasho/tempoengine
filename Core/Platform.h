#ifndef PLATFORM_H
#define PLATFORM_H

#include <glog/logging.h>
#include <SDL2/SDL_assert.h>

#include "TempoMath.h"

#include <memory>

#define TEMPO_NEW new

namespace TempoEngine
{
using U32 = unsigned int;
using U64 = unsigned long;

typedef Vector4 Color;

using ByteBuffer = std::unique_ptr<char[]>;

constexpr float OPAQUE = 1.0f;
constexpr float TRANSPARENT = 0.0f;

const Color gWhite(1.0f, 1.0f, 1.0f, OPAQUE);
const Color gBlack(0.0f, 0.0f, 0.0f, OPAQUE);
const Color gCyan(0.0f, 1.0f, 1.0f, OPAQUE);
const Color gRed(1.0f, 0.0f, 0.0f, OPAQUE);
const Color gGreen(0.0f, 1.0f, 0.0f, OPAQUE);
const Color gBlue(0.0f, 0.0f, 1.0f, OPAQUE);
const Color gYellow(1.0f, 1.0f, 0.0f, OPAQUE);
const Color gGray40(0.4f, 0.4f, 0.4f, OPAQUE);
const Color gGray65(0.65f, 0.65f, 0.65f, OPAQUE);
const Color gGray25(0.25f, 0.25f, 0.25f, OPAQUE);
const Color gTransparent(1.0f, 0.0f, 1.0f, TRANSPARENT);

template <typename BaseType, typename SubType>
BaseType* GenericObjectCreationFunction()
{
    return TEMPO_NEW SubType;
}

inline void LogSystemError()
{
#ifdef WIN32
    char* buffer[64];
    strerror_s(buffer[0], 64, errno);
    LOG(FATAL) << buffer;
#else
    LOG(FATAL) << strerror(errno);
#endif
}
}

#endif // PLATFORM_H
