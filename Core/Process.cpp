//
//  Process.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 5/20/14.
//
//

#include "Process.h"

namespace TempoEngine
{

Process::Process()
    : mState(State::UNINITIALIZED)
{
}

Process::~Process()
{
    if(mpChild) {
        mpChild->VOnAbort();
    }
}

StrongProcessPtr Process::RemoveChild()
{
    if(mpChild) {
        StrongProcessPtr result = mpChild;
        mpChild.reset();
        return result;
    }

    return StrongProcessPtr();
}

} //end of namespace