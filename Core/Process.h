//
//  Process.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/20/14.
//
//

#ifndef Engine_Process_h
#define Engine_Process_h

#include "Platform.h"
#include <memory>

namespace TempoEngine
{

class Process;
typedef std::shared_ptr<Process> StrongProcessPtr;
typedef std::weak_ptr<Process> WeakProcessPtr;

class Process
{
public:
    enum class State {
        UNINITIALIZED,
        REMOVED,
        RUNNING,
        PAUSED,
        SUCCEEDED,
        FAILED,
        ABORTED
    };

    Process();
    virtual ~Process();

    inline void Succeed();
    inline void Fail();
    inline void Pause();
    inline void UnPause();

    State GetState() const { return mState; }
    bool IsAlive() const { return (mState == State::RUNNING || mState == State::PAUSED); }
    bool IsDead() const { return (mState == State::SUCCEEDED || mState == State::FAILED || mState == State::ABORTED); }
    bool IsRemoved() const { return mState == State::REMOVED; }
    bool IsPaused() const { return mState == State::PAUSED; }

    inline void AttachChild(StrongProcessPtr pChild);
    StrongProcessPtr RemoveChild();
    StrongProcessPtr PeekChild() { return mpChild; }

protected:
    virtual void VOnInit() { mState = State::RUNNING; }
    virtual void VOnUpdate(U32 deltaMs) = 0;
    virtual void VOnSuccess() {}
    virtual void VOnFail() {}
    virtual void VOnAbort() {}

private:
    friend class ProcessManager;
    friend class ScriptProcess;

    State mState;
    StrongProcessPtr mpChild;

    void SetState(State state) { mState = state; }
};

inline void Process::Succeed()
{
    SDL_assert(mState == State::RUNNING || mState == State::PAUSED);
    mState = State::SUCCEEDED;
}

inline void Process::Fail()
{
    SDL_assert(mState == State::RUNNING || mState == State::PAUSED);
    mState = State::FAILED;
}

inline void Process::Pause()
{
    if(mState == State::RUNNING) {
        mState = State::PAUSED;
    } else {
        LOG(WARNING) << "Attempt for pausing not running process";
    }
}

inline void Process::UnPause()
{
    if(mState == State::PAUSED) {
        mState = State::RUNNING;
    } else {
        LOG(WARNING) << "Attempt for Unpausing not paused process";
    }
}

inline void Process::AttachChild(StrongProcessPtr pChild)
{
    if(mpChild) {
        mpChild->AttachChild(pChild);
    } else {
        mpChild = pChild;
    }
}

} //end of namespace

#endif
