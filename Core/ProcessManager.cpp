//
//  ProcessManager.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 5/20/14.
//
//

#include "ProcessManager.h"

namespace TempoEngine
{
ProcessManager::~ProcessManager()
{
    ClearAllProcesses();
}

U32 ProcessManager::UpdateProcesses(U32 deltaMs)
{
    unsigned short int successCount = 0;
    unsigned short int failCount = 0;

    auto it = mProcessList.begin();
    while(it != mProcessList.end()) {
        auto pCurrProcess = *it;
        auto thisIt = it++;

        if(pCurrProcess->GetState() == Process::State::UNINITIALIZED) {
            pCurrProcess->VOnInit();
        }

        if(pCurrProcess->GetState() == Process::State::RUNNING) {
            pCurrProcess->VOnUpdate(deltaMs);
        }

        if(pCurrProcess->IsDead()) {
            switch(pCurrProcess->GetState()) {
            case Process::State::SUCCEEDED: {
                pCurrProcess->VOnSuccess();
                auto pChild = pCurrProcess->RemoveChild();
                if(pChild) {
                    this->AttachProcess(pChild);
                } else {
                    ++successCount;
                }
                break;
            }
            case Process::State::FAILED: {
                pCurrProcess->VOnFail();
                ++failCount;
                break;
            }
            case Process::State::ABORTED: {
                pCurrProcess->VOnAbort();
                ++failCount;
                break;
            }
            default:
                break;
            }

            mProcessList.erase(thisIt);
        }
    }

    return ((successCount << 16) | failCount);
}

WeakProcessPtr ProcessManager::AttachProcess(StrongProcessPtr pProcess)
{
    mProcessList.push_front(pProcess);
    return WeakProcessPtr(pProcess);
}

void ProcessManager::AbortAllProcesses(bool immediate)
{
    auto it = mProcessList.begin();
    while(it != mProcessList.end()) {
        auto tempIt = it++;
        auto pProcess = *tempIt;
        if(pProcess->IsAlive()) {
            pProcess->SetState(Process::State::ABORTED);
            if(immediate) {
                pProcess->VOnAbort();
                mProcessList.erase(tempIt);
            }
        }
    }
}

void ProcessManager::ClearAllProcesses()
{
    mProcessList.clear();
}

} //end of namespace