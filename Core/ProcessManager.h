//
//  ProcessManager.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/20/14.
//
//

#ifndef __Engine__ProcessManager__
#define __Engine__ProcessManager__

#include "Process.h"

#include <list>

namespace TempoEngine
{
class ProcessManager
{
public:
    ~ProcessManager();

    U32 UpdateProcesses(U32 deltaMs);
    WeakProcessPtr AttachProcess(StrongProcessPtr pProcess);
    void AbortAllProcesses(bool immediate);

    U32 GetProcessCount() const { return mProcessList.size(); }

private:
    using ProcessList = std::list<StrongProcessPtr>;

    ProcessList mProcessList;

    void ClearAllProcesses();
};
}

#endif
