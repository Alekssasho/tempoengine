//
//  GLRenderer.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 6/2/14.
//
//

#include "GLRenderer.h"
#include "../platform_graphics.h"

namespace TempoEngine
{
namespace Renderer
{
    void GLRenderer::VShutdown()
    {
    }
    
    bool GLRenderer::VOnCreate()
    {
        this->SetState({true, true, GL_LESS, GL_CCW, GL_BACK}); //Default State
        return true;
    }
    
    void GLRenderer::SetState(GLState state)
    {
        //TODO: remember and undo last state
        if(state.depthTest) {
            TEMPO_GL_CHECK_ERROR(glEnable(GL_DEPTH_TEST));
            TEMPO_GL_CHECK_ERROR(glDepthFunc(state.depthFunc));
        } else {
            TEMPO_GL_CHECK_ERROR(glDisable(GL_DEPTH_TEST));
        }
        
        if(state.culling) {
            TEMPO_GL_CHECK_ERROR(glEnable(GL_CULL_FACE));
            TEMPO_GL_CHECK_ERROR(glFrontFace(state.frontFace));
            TEMPO_GL_CHECK_ERROR(glCullFace(state.cullFace));
        } else {
            TEMPO_GL_CHECK_ERROR(glDisable(GL_CULL_FACE));
        }
    }

    bool GLRenderer::VPreRender()
    {
        TEMPO_GL_CHECK_ERROR(glClearColor(mBackgroundColor.r,
                                          mBackgroundColor.g,
                                          mBackgroundColor.b,
                                          mBackgroundColor.a));
        TEMPO_GL_CHECK_ERROR(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

        return true;
    }

    bool GLRenderer::VPostRender()
    {
        return true;
    }

    //TODO: Implement alhpa and sky pass and return them here
    std::shared_ptr<IRenderState> GLRenderer::VPrepareAlphaPass()
    {
        return std::shared_ptr<IRenderState>();
    }

    std::shared_ptr<IRenderState> GLRenderer::VPrepareSkyBoxPass()
    {
        return std::shared_ptr<IRenderState>();
    }

    void GLRenderer::VDrawLine(const Vector3& from, const Vector3& to, const Color& color)
    {
        //TODO: Implement GLLineDrawer :D
    }
} //end of namespace Renderer
} //end of namespace TempoEngine