//
//  GLRenderer.h
//  Engine
//
//  Created by Aleksandar Angelov on 6/2/14.
//
//

#ifndef __Engine__GLRenderer__
#define __Engine__GLRenderer__

#include "../Interfaces.h"
#include "../platform_graphics.h"

namespace TempoEngine
{
namespace Renderer
{
    
    struct GLState
    {
        bool depthTest;
        bool culling;
        GLenum depthFunc;
        GLenum frontFace;
        GLenum cullFace;
    };
    
    class GLRenderer : public IRenderer
    {
    public:
        virtual void VSetBackgroundColor(const Color& color) override { mBackgroundColor = color; }
        virtual bool VOnCreate() override;
        virtual void VShutdown() override;
        virtual bool VPreRender() override;
        virtual bool VPostRender() override;
        void SetState(GLState state);

        //Maybe this needs to be unique_ptr
        virtual std::shared_ptr<IRenderState> VPrepareAlphaPass() override;
        virtual std::shared_ptr<IRenderState> VPrepareSkyBoxPass() override;
        virtual void VDrawLine(const Vector3& from, const Vector3& to, const Color& color) override;

    private:
        Color mBackgroundColor;
    };

} //end of namespace Renderer
} //end of namespace TempoEngine

#endif /* defined(__Engine__GLRenderer__) */
