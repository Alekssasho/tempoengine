#include "HardwareBufferManager.h"

#include <vector>
#include "IndexBuffer.h"
#include "vertex_buffer.h"

namespace TempoEngine
{
class HardwareBufferManagerImpl
{
public:
    HardwareBufferManagerImpl()
    {
    }

    VertexArrayPtr CreateVertexArray()
    {
        mVertexArrays.push_back(std::make_shared<HardwareVertexArray>());
        return mVertexArrays.back();
    }

    bool CreateVertexBuffer(size_t vertexSize, size_t numVertices, BufferUsage usage, const void* source)
    {
        VertexBufferPtr vBuff = std::make_shared<HardwareVertexBuffer>(vertexSize, numVertices, usage);
        vBuff->WriteData(source);
        mVertexBuffers.push_back(vBuff);
        return true;
    }

    bool CreateIndexBuffer(size_t vertexSize, size_t numVertices, BufferUsage usage, const void* source)
    {
        IndexBufferPtr iBuff = std::make_shared<HardwareIndexBuffer>(vertexSize, numVertices, usage);
        iBuff->WriteData(source);
        mIndexBuffers.push_back(iBuff);
        return true;
    }

    bool SetVertexAttribute(U32 index, int size, U32 type, unsigned char normalized, int stride, const void* pointer)
    {
        TEMPO_GL_CHECK_ERROR(glEnableVertexAttribArray(index));
        TEMPO_GL_CHECK_ERROR(glVertexAttribPointer(index, size, type, normalized, stride, pointer));
        return true;
    }

    void Clear()
    {
        mVertexArrays.clear();
        mIndexBuffers.clear();
        mVertexBuffers.clear();
    }

private:
    typedef std::shared_ptr<HardwareVertexBuffer> VertexBufferPtr;
    typedef std::shared_ptr<HardwareIndexBuffer> IndexBufferPtr;

    std::vector<VertexBufferPtr> mVertexBuffers;
    std::vector<IndexBufferPtr> mIndexBuffers;
    std::vector<VertexArrayPtr> mVertexArrays;
};

HardwareBufferManager& HardwareBufferManager::Instance()
{
    static HardwareBufferManager manager;
    return manager;
}

HardwareBufferManager::HardwareBufferManager()
    : mpImpl(new HardwareBufferManagerImpl())
{
}

HardwareBufferManager::~HardwareBufferManager()
{
    delete mpImpl;
}

VertexArrayPtr HardwareBufferManager::CreateVertexArray()
{
    return mpImpl->CreateVertexArray();
}

bool HardwareBufferManager::CreateVertexBufferForVArray(VertexArrayPtr pVArray, size_t vertexSize, size_t numVertices, BufferUsage usage, const void* source)
{
    pVArray->Bind();
    return mpImpl->CreateVertexBuffer(vertexSize, numVertices, usage, source);
}

bool HardwareBufferManager::CreateIndexBufferForVArray(VertexArrayPtr pVArray, size_t vertexSize, size_t numVertices, BufferUsage usage, const void* source)
{
    pVArray->Bind();
    return mpImpl->CreateIndexBuffer(vertexSize, numVertices, usage, source);
}

bool HardwareBufferManager::SetVertexAttributeForVArray(VertexArrayPtr pVArray, U32 index, int size, U32 type, unsigned char normalized, int stride, const void* pointer)
{
    pVArray->Bind();
    return mpImpl->SetVertexAttribute(index, size, type, normalized, stride, pointer);
}

void HardwareBufferManager::Clear()
{
    mpImpl->Clear();
}

} //end of namespace
