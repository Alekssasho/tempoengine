#ifndef HARDWAREBUFFERMANAGER_H
#define HARDWAREBUFFERMANAGER_H

#include <memory>
#include "../Utilities.h"
#include "vertexarray.h"
#include "../Platform.h"

namespace TempoEngine
{

typedef std::shared_ptr<HardwareVertexArray> VertexArrayPtr;

class HardwareBufferManagerImpl;

class HardwareBufferManager : public Utility::Noncopyable
{
public:
    ~HardwareBufferManager();

    static HardwareBufferManager& Instance();

    VertexArrayPtr CreateVertexArray();
    bool CreateVertexBufferForVArray(VertexArrayPtr pVArray, size_t vertexSize, size_t numVertices, BufferUsage usage, const void* source);
    bool CreateIndexBufferForVArray(VertexArrayPtr pVArray, size_t vertexSize, size_t numVertices, BufferUsage usage, const void* source);
    bool SetVertexAttributeForVArray(VertexArrayPtr pVArray, U32 index, int size, U32 type, unsigned char normalized, int stride, const void* pointer);

    void Clear();

private:
    HardwareBufferManager();

    HardwareBufferManagerImpl* mpImpl;
};

} //end of namespace

#endif // HARDWAREBUFFERMANAGER_H
