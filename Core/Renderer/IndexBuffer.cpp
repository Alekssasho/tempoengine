#include "IndexBuffer.h"
#include "../Platform.h"

namespace TempoEngine
{
HardwareIndexBuffer::HardwareIndexBuffer(size_t vertexSize, size_t numVertices, BufferUsage usage)
    : mID(0)
    , mIndexSize(vertexSize)
    , mNumIndecies(numVertices)
    , mUsage(usage)
{
    TEMPO_GL_CHECK_ERROR(glGenBuffers(1, &mID));
    if(!mID) {
        LOG(FATAL) << "Failed to create index VBO ";
        return;
    }
    TEMPO_GL_CHECK_ERROR(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mID));
    TEMPO_GL_CHECK_ERROR(glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndexSize * mNumIndecies, NULL, GetGLBufferUsage(mUsage)));
}

HardwareIndexBuffer::~HardwareIndexBuffer()
{
    TEMPO_GL_CHECK_ERROR(glDeleteBuffers(1, &mID));
}

void HardwareIndexBuffer::WriteData(const void* source)
{
    TEMPO_GL_CHECK_ERROR(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mID));
    TEMPO_GL_CHECK_ERROR(glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndexSize * mNumIndecies, source, GetGLBufferUsage(mUsage)));
}

} //end of namespace
