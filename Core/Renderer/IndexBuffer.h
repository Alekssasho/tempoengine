#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

#include "../platform_graphics.h"

namespace TempoEngine
{
class HardwareIndexBuffer
{
public:
    HardwareIndexBuffer(size_t indexSize, size_t numIndecies, BufferUsage usage);
    ~HardwareIndexBuffer();

    void WriteData(const void* source);

private:
    GraphicsObjectID mID;
    size_t mIndexSize;
    size_t mNumIndecies;
    BufferUsage mUsage;
};
} // end of namespace

#endif // INDEXBUFFER_H
