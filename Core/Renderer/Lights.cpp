//
//  Lights.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 5/30/14.
//
//

#include "Lights.h"
#include "Scene.h"

#include <algorithm>

namespace TempoEngine
{
namespace Renderer
{

    LightManager::LightManager()
    {
        mLights.reserve(MAXIMUM_LIGHT_SUPPORTED);
    }

    void LightManager::CalcLighting(Scene* pScene)
    {
        //TODO: this is called when starting to render scene, some pre light stuff maybe here
    }
    
    int LightManager::GetLightCount(const SceneNode& node)
    {
        //TODO: check for light visibility for this specific node
        return mLights.size();
    }

    void LightManager::CalcLighting(LightingBuffer& pBuffer, SceneNode* pNode)
    {
        int count = this->GetLightCount(*pNode);
        pBuffer.isDirLightAvailable = false;
        pBuffer.numPointLights = pBuffer.numSpotLights = 0;
        for(int i = 0; i < count; ++i) {
            auto& props = mLights[i]->GetLightProperties();
            switch (props.type) {
                case LightType::Directional:
                    pBuffer.dirLight = props;
                    pBuffer.isDirLightAvailable = true;
                    break;
                case LightType::Point:
                    if (pBuffer.numPointLights < POINT_LIGHT_SUPPORTED) {
                        pBuffer.pointLights[pBuffer.numPointLights++] = props;
                    }
                    break;
                case LightType::Spot:
                    if (pBuffer.numSpotLights < SPOT_LIGHT_SUPPORTED) {
                        pBuffer.spotLights[pBuffer.numSpotLights++] = props;
                    }
                    break;
            }
        }
    }

} //end of namespace Renderer=
} //end of namespace TempoEngine