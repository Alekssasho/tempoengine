//
//  Lights.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/30/14.
//
//

#ifndef __Engine__Lights__
#define __Engine__Lights__

#include "SceneNodes.h"

namespace TempoEngine
{
namespace Renderer
{
    constexpr int POINT_LIGHT_SUPPORTED = 2;
    constexpr int SPOT_LIGHT_SUPPORTED = 2;
    //Directional light + max point + max spot lights
    constexpr int MAXIMUM_LIGHT_SUPPORTED = 1 + POINT_LIGHT_SUPPORTED + SPOT_LIGHT_SUPPORTED;
    
    enum class LightType
    {
        Directional,
        Point,
        Spot
    };
    
    struct LightProperties
    {
        LightType type;
        Vector3 color;
        float ambientIntensity;
        float diffuseIntensity;
        Vector3 direction;
        Vector3 position;
        struct
        {
            float constant;
            float linear;
            float exp;
        } attenuation;
        float cutoff;
        
        LightProperties()
        : type(LightType::Directional),
        color(gOnes),
        ambientIntensity(0),
        diffuseIntensity(0),
        direction(gForward),
        position(gZero),
        cutoff(0)
        {
            attenuation.constant = 1;
            attenuation.linear = 0;
            attenuation.exp = 0;
        }
    };
    
    struct LightingBuffer
    {
        LightProperties dirLight;
        U32 numPointLights;
        LightProperties pointLights[POINT_LIGHT_SUPPORTED];
        U32 numSpotLights;
        LightProperties spotLights[SPOT_LIGHT_SUPPORTED];
        bool isDirLightAvailable;
    };

    class LightNode : public SceneNode
    {
    public:
        LightNode(ActorId actorId, std::string name, const LightProperties& prop, const Transform* pTransf)
        : SceneNode(actorId, std::move(name), RenderPass::NotRendered, pTransf)
        , mLightProp(prop)
        {}

        virtual bool VIsLight() const override { return true; }
        
        const LightProperties& GetLightProperties() const { return mLightProp; }

    protected:
        LightProperties mLightProp;
    };

    class LightManager
    {
    public:
        LightManager();

        void CalcLighting(Scene* pScene);
        void CalcLighting(LightingBuffer& buffer, SceneNode* pNode);

        int GetLightCount(const SceneNode& node);

    protected:
        LightPtrVector mLights;
    private:
        friend class Scene;
    };
}
}

#endif /* defined(__Engine__Lights__) */
