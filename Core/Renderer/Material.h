//
//  Material.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/28/14.
//
//

#ifndef __Engine__Material__
#define __Engine__Material__

#include "../Platform.h"

namespace TempoEngine
{
namespace Renderer
{

    class Material
    {
    public:
        Material()
            : mDiffuse{ gWhite }
            , mAmbient{ Color(0.1f, 0.1f, 0.1f, OPAQUE) }
            , mSpecular{ gWhite }
            , mEmissive{ gBlack }
            , mPower{ 0.0f }
        {
        }

        void SetAmbient(const Color& color) { mAmbient = color; }
        const Color GetAmbient() const { return mAmbient; }

        void SetDiffuse(const Color& color) { mDiffuse = color; }
        const Color GetDiffuse() const { return mDiffuse; }

        void SetSpecular(const Color& color, const float power)
        {
            mSpecular = color;
            mPower = power;
        }
        void GetSpecular(Color& color, float& power) const
        {
            color = mSpecular;
            power = mPower;
        }

        void SetEmissive(const Color& color) { mEmissive = color; }
        const Color GetEmissive() const { return mEmissive; }

        void SetAlpha(const float alpha) { mDiffuse.a = alpha; }
        bool HasAlpha() const { return this->GetAlpha() != OPAQUE; }
        float GetAlpha() const { return mDiffuse.a; }

    private:
        Color mDiffuse;
        Color mAmbient;
        Color mSpecular;
        Color mEmissive;
        float mPower;
    };
} //end of namespace Renderer
} //end of namespace TempoEngine

#endif /* defined(__Engine__Material__) */
