//
//  Scene.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 5/29/14.
//
//

#include "Scene.h"
#include "Lights.h"
#include "../EventManager/CommonEvents.h"
#include <functional>

namespace TempoEngine
{
namespace Renderer
{
    Scene::Scene(const shared_ptr<IRenderer>& pRenderer)
        : mRoot(TEMPO_NEW RootNode())
        , mRenderer(pRenderer)
        , mLightManager(TEMPO_NEW LightManager())
    {
        EventManager *pEventManager = EventManager::Get();
        pEventManager->AddListener(fastdelegate::MakeDelegate(this, &Scene::NewRenderComponentDelegate), EventNewRenderComponentData::kEventType);
    }

    Scene::~Scene()
    {
    }

    bool Scene::OnRender()
    {
        if(mRoot && mCamera) {
            mCamera->SetViewTransform(this);

            mLightManager->CalcLighting(this);

            if(mRoot->VPreRender(this)) {
                mRoot->VRender(this);
                mRoot->VRenderChildren(this);
                mRoot->VPostRender(this);
            }

            this->RenderAlphaPass();
        }

        return true;
    }

    bool Scene::OnCreate()
    {
        if(!mRoot) {
            return true;
        }
        
        mRenderer->VOnCreate();

        return mRoot->VOnCreate(this);
    }

    bool Scene::OnUpdate(const U32 deltaMiliseconds)
    {
        if(!mRoot) {
            return true;
        }

        return mRoot->VOnUpdate(this, deltaMiliseconds);
    }

    shared_ptr<ISceneNode> Scene::FindActor(ActorId id)
    {
        auto findIt = mActorMap.find(id);
        if(findIt == mActorMap.end()) {
            return shared_ptr<ISceneNode>();
        }

        return findIt->second;
    }

    bool Scene::AddChild(ActorId id, shared_ptr<ISceneNode> kid)
    {
        if(id != INVALID_ACTOR_ID) {
            mActorMap[id] = kid;
        }

        if(kid->VIsLight() && mLightManager->mLights.size() + 1 <= MAXIMUM_LIGHT_SUPPORTED) {
            mLightManager->mLights.push_back(std::static_pointer_cast<LightNode>(kid));
        }

        return mRoot->VAddChild(kid);
    }

    bool Scene::RemoveChild(ActorId id)
    {
        if(id == INVALID_ACTOR_ID) {
            return false;
        }

        shared_ptr<ISceneNode> kid = this->FindActor(id);
        if(kid->VIsLight()) {
            //            mLightManager->mLights.remove(std::static_pointer_cast<LightNode>(kid));
            auto& lights = mLightManager->mLights;
            auto findIt = std::find(lights.begin(), lights.end(), std::static_pointer_cast<LightNode>(kid));
            if(findIt != lights.end())
                lights.erase(findIt);
        }

        mActorMap.erase(id);
        return mRoot->VRemoveChild(id);
    }

    void Scene::RenderAlphaPass()
    {
        shared_ptr<IRenderState> alphaPass = mRenderer->VPrepareAlphaPass();

        std::sort(mAlphaSceneNodes.begin(), mAlphaSceneNodes.end(), std::greater<AlphaSceneNode>());
        for(const auto& node : mAlphaSceneNodes) {
            this->PushAndSetMatrix(node.mConcat);

            node.mpNode->VRender(this);

            this->PopMatrix();
        }

        mAlphaSceneNodes.clear();
    }

    void Scene::NewRenderComponentDelegate(IEventDataPtr pEventData)
    {
        auto pData = std::static_pointer_cast<EventNewRenderComponentData>(pEventData);
        auto actorId = pData->GetActorId();
        auto sceneNode = pData->GetSceneNode();
        
        if (!sceneNode->VOnCreate(this)) {
            LOG(ERROR) << "Failed to create new scene node from render component";
            return;
        }
        
        this->AddChild(actorId, sceneNode);
    }

    void Scene::DestroyActorDelegate(IEventDataPtr pEventData)
    {
    }

} //end of namespace Renderer
} //end of namespace TempoEngine