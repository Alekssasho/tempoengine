//
//  Scene.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/29/14.
//
//

#ifndef __Engine__Scene__
#define __Engine__Scene__

#include "../Interfaces.h"
#include "SceneNodes.h"
#include "../EventManager/EventManager.h"
#include <map>

namespace TempoEngine
{
namespace Renderer
{
    using std::shared_ptr;

    typedef std::map<ActorId, shared_ptr<ISceneNode>> SceneActorMap;
    typedef std::vector<AlphaSceneNode> AlphaSceneNodes;

    class LightNode;
    class LightManager;

    class IRenderer;

    class Scene
    {
    public:
        Scene(const shared_ptr<IRenderer>& pRenderer);
        virtual ~Scene();

        bool OnRender();
        bool OnCreate();
        bool OnUpdate(const U32 deltaMiliseconds);
        shared_ptr<ISceneNode> FindActor(ActorId id);
        bool AddChild(ActorId id, shared_ptr<ISceneNode> kid);
        bool RemoveChild(ActorId id);

        void SetCamera(shared_ptr<CameraNode> camera) { mCamera = camera; }
        shared_ptr<CameraNode> GetCamera() const { return mCamera; }

        void PushAndSetMatrix(const Matrix4& toWorld)
        {
            mMatrixStack.Push();
            mMatrixStack.MultiplyTopMatrix(toWorld);
        }

        void PopMatrix()
        {
            mMatrixStack.Pop();
        }

        const Matrix4 GetTopMatrix()
        {
            return mMatrixStack.GetTopMatrix();
        }

        LightManager* GetLightManager() const { return mLightManager.get(); }
        void AddAlphaSceneNode(AlphaSceneNode& node) { mAlphaSceneNodes.push_back(std::move(node)); } //Not sure for std::move

        shared_ptr<IRenderer> GetRenderer() const { return mRenderer; }

        // Event Delegates
        void NewRenderComponentDelegate(IEventDataPtr pEventData);

        void DestroyActorDelegate(IEventDataPtr pEventData);

    protected:
        shared_ptr<SceneNode> mRoot;
        shared_ptr<CameraNode> mCamera;
        shared_ptr<IRenderer> mRenderer;

        MatrixStack mMatrixStack;

        AlphaSceneNodes mAlphaSceneNodes;
        SceneActorMap mActorMap;
        std::unique_ptr<LightManager> mLightManager;

        void RenderAlphaPass();
    };

    class ScreenElementScene : public IScreenElement
    {
    private:
        Scene mScene;
    public:
        ScreenElementScene(const std::shared_ptr<IRenderer>& renderer)
            : mScene(renderer)
        {
        }

        void VOnUpdate(unsigned long deltaMs) override { mScene.OnUpdate(deltaMs); }
        bool VOnCreate() override { return mScene.OnCreate(); }
        bool VOnRender() override { return mScene.OnRender(); }
        int VGetZOrder() const override { return 0; }
        void VSetZOrder(int zOrder) override {}
        bool VIsVisible() const override { return true; }
        void VSetVisible(bool visible) override {}
        virtual bool VAddChild(ActorId id, std::shared_ptr<Renderer::ISceneNode> kid) { return mScene.AddChild(id, kid); }
        bool VOnMessage(InputEvent event) override { return false; }
        Scene& GetScene() { return mScene; }
    
    };

} //end of namespace Renderer
} //end of namespace TempoEngine

#endif /* defined(__Engine__Scene__) */
