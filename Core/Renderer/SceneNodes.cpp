//
//  SceneNodes.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 5/29/14.
//
//

#include "SceneNodes.h"
#include "Scene.h"
#include "../TempoMath.h"
#include <algorithm>

namespace TempoEngine
{
namespace Renderer
{

    Matrix4 SceneNodeProperties::GetToWorld() const
    {
        return mpTransform->Matrix();
    }

    Matrix4 SceneNodeProperties::GetFromWorld() const
    {
        //TODO: better implement this
        return glm::inverse(this->GetToWorld());
    }

    using std::shared_ptr;

    SceneNode::SceneNode(ActorId actorId,
                         std::string name,
                         RenderPass renderPass,
                         const Transform* pTransf)
        : mpParent(nullptr)
    {
        mProps.name = std::move(name);
        mProps.mActorId = actorId;
        mProps.mRenderPass = renderPass;
        mProps.mAlphaType = AlphaType::Opaque;
        mProps.mpTransform = pTransf;
        this->SetRadius(0);
    }

    SceneNode::~SceneNode()
    {
    }

    const Vector3 SceneNode::GetWorldPosition() const
    {
        Vector3 pos = this->GetPosition();
        if(mpParent) {
            pos += mpParent->GetWorldPosition();
        }
        return pos;
    }

    bool SceneNode::VOnUpdate(Scene* pScene, const U32 elapsedMs)
    {
        for(const auto& pChild : mChildren)
            pChild->VOnUpdate(pScene, elapsedMs);
        return true;
    }

    bool SceneNode::VOnCreate(Scene* pScene)
    {
        for(const auto& pChild : mChildren)
            pChild->VOnCreate(pScene);
        return true;
    }

    bool SceneNode::VPreRender(Scene* pScene)
    {
        pScene->PushAndSetMatrix(mProps.GetToWorld());
        return true;
    }

    bool SceneNode::VPostRender(Scene* pScene)
    {
        pScene->PopMatrix();
        return true;
    }

    bool SceneNode::VIsVisible(Scene* pScene) const
    {
        const auto& cameraMatrix = pScene->GetCamera()->GetView();
        Vector3 pos = GetTranslation(this->VGet()->GetToWorld());

        pos = (cameraMatrix * Vector4(pos, 1.0f)).xyz();

        const auto& frustrum = pScene->GetCamera()->GetFrustum();
        return frustrum.Inside(pos, this->VGet()->GetRadius());
    }

    bool SceneNode::VRenderChildren(Scene* pScene)
    {
        for(const auto& pChild : mChildren) {
            if(pChild->VPreRender(pScene)) {
                if(pChild->VIsVisible(pScene)) {
                    float alpha = pChild->VGet()->GetAlpha();
                    if(alpha == OPAQUE) {
                        pChild->VRender(pScene);
                    } else if(alpha != TRANSPARENT) {
                        AlphaSceneNode asn;
                        asn.mpNode = pChild;
                        asn.mConcat = pScene->GetTopMatrix();

                        const Vector4& worldPos = Vector4(TempoEngine::GetTranslation(asn.mConcat), 1.0f);
                        const Matrix4& fromWorld = pScene->GetCamera()->VGet()->GetFromWorld();

                        asn.mScreenZ = (fromWorld * worldPos).z;
                        pScene->AddAlphaSceneNode(asn);
                    }

                    pChild->VRenderChildren(pScene);
                }
            }
            pChild->VPostRender(pScene);
        }

        return true;
    }

    bool SceneNode::VAddChild(shared_ptr<ISceneNode> ikid)
    {
        mChildren.push_back(ikid);
        auto kid = std::static_pointer_cast<SceneNode>(ikid);

        kid->mpParent = this;

        Vector3 kidPos = GetTranslation(kid->VGet()->GetToWorld());

        float newRadius = kidPos.length() + kid->VGet()->GetRadius();

        if(newRadius > mProps.mRadius) {
            mProps.mRadius = newRadius;
        }

        return true;
    }

    bool SceneNode::VRemoveChild(ActorId id)
    {
        auto findIt = std::find_if(std::begin(mChildren), std::end(mChildren), [&id](const shared_ptr<ISceneNode>& pChild) {
            const auto pPros = pChild->VGet();
            return (pPros->GetActorId() != INVALID_ACTOR_ID && id == pPros->GetActorId());
        });

        if(findIt != std::end(mChildren)) {
            mChildren.erase(findIt);
            return true;
        }
        return false;
    }

    bool operator>(const AlphaSceneNode& lhs, const AlphaSceneNode& rhs) { return lhs.mScreenZ > rhs.mScreenZ; }

    RootNode::RootNode()
        : SceneNode(INVALID_ACTOR_ID, "RootNode", RenderPass::Static, &gDefaultTransform)
    {
        mChildren.reserve(4); //need to be some constant of sort

        shared_ptr<SceneNode> staticGroup = std::make_shared<SceneNode>(INVALID_ACTOR_ID, "StaticGroup", RenderPass::Static, &gDefaultTransform);
        shared_ptr<SceneNode> actorGroup = std::make_shared<SceneNode>(INVALID_ACTOR_ID, "ActorGroup", RenderPass::Actor, &gDefaultTransform);
        shared_ptr<SceneNode> skyGroup = std::make_shared<SceneNode>(INVALID_ACTOR_ID, "SkyGroup", RenderPass::Sky, &gDefaultTransform);
        shared_ptr<SceneNode> notRenderedGroup = std::make_shared<SceneNode>(INVALID_ACTOR_ID, "NotRenderedGroup", RenderPass::NotRendered, &gDefaultTransform);

        mChildren.push_back(staticGroup);
        mChildren.push_back(actorGroup);
        mChildren.push_back(skyGroup);
        mChildren.push_back(notRenderedGroup);
    }

    bool RootNode::VAddChild(shared_ptr<ISceneNode> kid)
    {
        auto pass = kid->VGet()->GetRenderPass();
        auto intPass = static_cast<unsigned>(pass);
        if(intPass >= mChildren.size() || !mChildren[intPass]) {
            LOG(ERROR) << "There is no such render pass";
            return false;
        }

        return mChildren[intPass]->VAddChild(kid);
    }

    bool RootNode::VRemoveChild(ActorId id)
    {
        bool result = false;

        for(const auto& child : mChildren) {
            if(child->VRemoveChild(id)) {
                result = true;
            }
        }

        return result;
    }

    bool RootNode::VRenderChildren(Scene* pScene)
    {
        for(const auto& child : mChildren) {
            switch(child->VGet()->GetRenderPass()) {
            case RenderPass::Static:
            case RenderPass::Actor:
                child->VRenderChildren(pScene);
                break;
            case RenderPass::Sky: {
                auto skyPass = pScene->GetRenderer()->VPrepareSkyBoxPass();
                child->VRenderChildren(pScene);
                break;
            }
            default:
                break;
            }
        }

        return true;
    }

    CameraNode::CameraNode(const Vector3& position, const Frustum& frustum)
        : SceneNode(INVALID_ACTOR_ID, "CameraNode", RenderPass::Static, nullptr)
        , mFrustum(frustum)
        , mCameraOffsetVector{ 0.0f, 1.0f, -10.0f }
        , mActive(true)
        , mDebugCamera(false)
    {
        Transform* pTransform = new Transform;
        pTransform->SetPosition(position);
        mProps.mpTransform = pTransform;
    }
    
    CameraNode::~CameraNode()
    {
        delete mProps.mpTransform;
    }

    bool CameraNode::VRender(Scene* pScene)
    {
        if(mDebugCamera) {
            //TODO: Render Frustum
        }
        return true;
    }

    bool CameraNode::VOnCreate(Scene* pScene)
    {
        mProjection = glm::perspective(mFrustum.mFov.ValueRadians(), mFrustum.mAspect, mFrustum.mNear, mFrustum.mFar);

        return true;
    }

    Matrix4 CameraNode::GetWorldViewProjection(Scene* pScene)
    {
        //World View Projection matrix
        return mProjection * mView * pScene->GetTopMatrix();
    }

    bool CameraNode::SetViewTransform(Scene* pScene)
    {
        if(mpTarget) {
            //Set center of view matrix to be position of the target
            //And also orientate camera offset vector to the orientation of the target, could be optional maybe

            const auto& yawPitchRoll = mpTarget->VGet()->GetYawPitchRoll();
            const auto& position = mpTarget->VGet()->GetPosition();
            Vector3 atWorld = glm::orientate3(yawPitchRoll) * mCameraOffsetVector;

            Vector3 pos = position + atWorld;
            mView = glm::lookAt(pos, position, gUp);
        }
        //TODO: implement non following camera behaiviour here
        return true;
    }

} //end of namespace Renderer
} //end of namespace TempoEngine