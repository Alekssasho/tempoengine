//
//  SceneNodes.h
//  Engine
//
//  Created by Aleksandar Angelov on 5/29/14.
//
//

#ifndef __Engine__SceneNodes__
#define __Engine__SceneNodes__

#include "Material.h"
#include "../Interfaces.h"
#include <utility>
#include <vector>

namespace TempoEngine
{
namespace Renderer
{
    enum class AlphaType {
        Opaque,
        Texture,
        Material,
        Vertex
    };

    class SceneNodeProperties
    {
    public:
        const ActorId& GetActorId() const { return mActorId; }
        Matrix4 GetToWorld() const;
        Matrix4 GetFromWorld() const;
        const Vector3& GetPosition() const { return mpTransform->Position(); }
        const Vector3& GetYawPitchRoll() const { return mpTransform->Rotation(); }
        const Vector3& GetScale() const { return mpTransform->Scale(); }
        std::pair<Matrix4, Matrix4> GetTransform() const { return std::make_pair(this->GetToWorld(), this->GetFromWorld()); }

        bool HasAlpha() const { return mMaterial.HasAlpha(); }
        float GetAlpha() const { return mMaterial.GetAlpha(); }
        AlphaType GetAlphaType() const { return mAlphaType; }

        RenderPass GetRenderPass() const { return mRenderPass; }
        float GetRadius() const { return mRadius; }
        const Material& GetMaterial() const { return mMaterial; }

    protected:
        ActorId mActorId;
        const Transform* mpTransform;
        float mRadius;
        std::string name;
        RenderPass mRenderPass;
        Material mMaterial;
        AlphaType mAlphaType;

        void SetAlpha(const float alpha)
        {
            mAlphaType = AlphaType::Material;
            mMaterial.SetAlpha(alpha);
        }

    private:
        friend class SceneNode;
        friend class CameraNode;
    };

    typedef std::vector<std::shared_ptr<ISceneNode>> SceneNodeList;

    class SceneNode : public ISceneNode
    {
    public:
        SceneNode(ActorId actorId,
                  std::string name,
                  RenderPass renderPass,
                  const Transform* pTransform);

        virtual ~SceneNode();
        const SceneNodeProperties* const VGet() const override { return &mProps; }
        bool VOnUpdate(Scene* pScene, const U32 elapsedMs) override;
        bool VOnCreate(Scene* pScene) override;
        bool VPreRender(Scene* pScene) override;
        bool VIsVisible(Scene* pScene) const override;
        bool VRender(Scene* pScene) override { return true; }
        bool VRenderChildren(Scene* pScene) override;
        bool VPostRender(Scene* pScene) override;
        bool VAddChild(std::shared_ptr<ISceneNode> kid) override;
        bool VRemoveChild(ActorId id) override;

        bool VIsLight() const override { return false; }

        void SetAlpha(float alpha) { mProps.SetAlpha(alpha); }
        float GetAlpha() const { return mProps.GetAlpha(); }

        Vector3 GetPosition() const { return mProps.GetPosition(); }
//        void SetPosition(const Vector3& vec) { mProps.mpTransform->SetPosition(vec); }

        const Vector3 GetWorldPosition() const;

        Vector3 GetDirection() const { return TempoEngine::GetDirection(mProps.GetToWorld()); }

        void SetRadius(const float radius) { mProps.mRadius = radius; }
        void SetMaterial(const Material& mat) { mProps.mMaterial = mat; }

    protected:
        SceneNodeList mChildren;
        SceneNode* mpParent;
        SceneNodeProperties mProps;

    private:
        friend class Scene;
    };

    struct AlphaSceneNode
    {
        std::shared_ptr<ISceneNode> mpNode;
        Matrix4 mConcat;
        float mScreenZ;
    };

    //We need to sort Alpha nodes in descending order so that we draw them from back to front
    bool operator>(const AlphaSceneNode& lhs, const AlphaSceneNode& rhs);

    class RootNode : public SceneNode
    {
    public:
        RootNode();
        virtual bool VAddChild(std::shared_ptr<ISceneNode> kid) override;
        virtual bool VRemoveChild(ActorId id) override;
        virtual bool VRenderChildren(Scene* pScene) override;
        virtual bool VIsVisible(Scene* pScene) const override { return true; }
    };

    class CameraNode : public SceneNode
    {
    public:
        CameraNode(const Vector3& position, const Frustum& frustum);
        ~CameraNode();

        virtual bool VRender(Scene* pScene) override;
        virtual bool VOnCreate(Scene* pScene) override;
        virtual bool VIsVisible(Scene* pScene) const override { return mActive; }

        const Frustum& GetFrustum() const { return mFrustum; }
        void SetTarget(std::shared_ptr<SceneNode> pTarget) { mpTarget = pTarget; }
        void ClearTarget() { mpTarget.reset(); }
        std::shared_ptr<SceneNode> GetTarget() const { return mpTarget; }

        Matrix4 GetWorldViewProjection(Scene* pScene);
        bool SetViewTransform(Scene* pScene);

        Matrix4 GetProjection() const { return mProjection; }
        Matrix4 GetView() const { return mView; }

        void SetCameraOffset(const Vector3& cameraOffset) { mCameraOffsetVector = cameraOffset; }

    protected:
        Frustum mFrustum;
        Matrix4 mProjection;
        Matrix4 mView;
        std::shared_ptr<SceneNode> mpTarget;
        Vector3 mCameraOffsetVector;
        bool mActive;
        bool mDebugCamera;

    private:
    };

} //end of namespace Renderer
} //end of namespace TempoEngine
#endif /* defined(__Engine__SceneNodes__) */
