#include <fstream>
#include "shaderprogram.h"
#include "../Platform.h"

namespace TempoEngine
{
namespace Renderer
{

    GLuint ShaderProgram::CompileShader(const Path& fPath, ShaderType type)
    {
        GLuint shaderID;
        TEMPO_GL_CHECK_ERROR(shaderID = glCreateShader(static_cast<GLenum>(type)));

        std::string filePath(fPath.String());

        std::ifstream fileStream(filePath.c_str());
        if(!fileStream.good())
            perror("Error with fileStream");
        std::string shader((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());
        fileStream.close();

        LOG(INFO) << "Compiling shader : " << filePath;

        const char* shaderSource = shader.c_str();
        TEMPO_GL_CHECK_ERROR(glShaderSource(shaderID, 1, &shaderSource, NULL));
        TEMPO_GL_CHECK_ERROR(glCompileShader(shaderID));

        GLint status;
        TEMPO_GL_CHECK_ERROR(glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status));
        if(status == GL_FALSE) {
            int infoLength;

            TEMPO_GL_CHECK_ERROR(glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLength));
            GLchar* info = new GLchar[infoLength];
            TEMPO_GL_CHECK_ERROR(glGetShaderInfoLog(shaderID, infoLength, NULL, info));
            LOG(FATAL) << "Error while compiling shader " << filePath << "\nWith Error: \n" << info;
            delete[] info;

            return -1;
        }

        TEMPO_GL_CHECK_ERROR(glAttachShader(mShaderProgramID, shaderID));

        return shaderID;
    }

    ShaderProgram::ShaderProgram()
    {
        TEMPO_GL_CHECK_ERROR(mShaderProgramID = glCreateProgram());
    }

    void ShaderProgram::Finalize()
    {
        LOG(INFO) << "Start linking shader";
        TEMPO_GL_CHECK_ERROR(glLinkProgram(mShaderProgramID));

        GLint status;
        TEMPO_GL_CHECK_ERROR(glGetProgramiv(mShaderProgramID, GL_LINK_STATUS, &status));
        if(status == GL_FALSE) {
            int infoLength;
            TEMPO_GL_CHECK_ERROR(glGetProgramiv(mShaderProgramID, GL_INFO_LOG_LENGTH, &infoLength));
            GLchar* info = new GLchar[infoLength];
            TEMPO_GL_CHECK_ERROR(glGetProgramInfoLog(mShaderProgramID, infoLength, NULL, info));
            LOG(ERROR) << "Error while compiling shader program: \n" << info;
            delete[] info;
        }
        LOG(INFO) << "Shader linked succesfull";
    }

    ShaderProgram::~ShaderProgram()
    {
        TEMPO_GL_CHECK_ERROR(glDeleteProgram(mShaderProgramID));
    }

    void ShaderProgram::ActiveProgram()
    {
        TEMPO_GL_CHECK_ERROR(glUseProgram(mShaderProgramID));
    }

    GraphicsObjectID ShaderProgram::GetUniformLocation(const std::string& name)
    {
        GraphicsObjectID id;
        TEMPO_GL_CHECK_ERROR(id = glGetUniformLocation(mShaderProgramID, name.c_str()));

        assert(id != INVALID_GRAPHICS_ID);
        return id;
    }

    void ShaderProgram::SetMatrix(const std::string& name, const Matrix4& matrix)
    {
        TEMPO_GL_CHECK_ERROR(glUniformMatrix4fv(this->GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(matrix)));
    }

    void ShaderProgram::SetSampler(const std::string& name, int sampleNum)
    {
        TEMPO_GL_CHECK_ERROR(glUniform1i(this->GetUniformLocation(name), sampleNum));
    }

    void ShaderProgram::ValidateProgram()
    {
#ifdef NDEBUG
        TEMPO_GL_CHECK_ERROR(glUseProgram(mShaderProgramID));
        TEMPO_GL_CHECK_ERROR(glValidateProgram(mShaderProgramID));

        GLint status;
        TEMPO_GL_CHECK_ERROR(glGetProgramiv(mShaderProgramID, GL_VALIDATE_STATUS, &status));
        if(status == GL_FALSE) {
            int infoLength;
            TEMPO_GL_CHECK_ERROR(glGetProgramiv(mShaderProgramID, GL_INFO_LOG_LENGTH, &infoLength));
            GLchar* info = new GLchar[infoLength];
            TEMPO_GL_CHECK_ERROR(glGetProgramInfoLog(mShaderProgramID, infoLength, NULL, info));
            LOG(ERROR) << "Error while validating shader program: \n" << info;
            delete[] info;
        } else {
            LOG(INFO) << "Validation Succesfull";
        }
#endif
    }

} //end of namespace Renderer
} //end of namespace
