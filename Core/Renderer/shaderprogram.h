#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <string>
#include "../platform_graphics.h"
#include "../TempoMath.h"
#include "ResourceLoading.h"
#include <boost/noncopyable.hpp>

namespace TempoEngine
{
namespace Renderer
{
    enum class ShaderType : GLenum {
        VERTEX = GL_VERTEX_SHADER,
        FRAGMENT = GL_FRAGMENT_SHADER,
        GEOMETRY = GL_GEOMETRY_SHADER
    };

    class Scene;
    class SceneNode;

    class ShaderProgram : boost::noncopyable
    {
    public:
        ShaderProgram();
        virtual ~ShaderProgram();

        void ActiveProgram();
        void SetMatrix(const std::string& name, const Matrix4& matrix);
        void SetSampler(const std::string& name, int sampleNum);
        void ValidateProgram();

        virtual bool SetupRender(Scene* pScene, SceneNode* pNode) = 0;

    protected:
        GraphicsObjectID CompileShader(const Path& filePath, ShaderType type);
        void Finalize();
        GraphicsObjectID GetUniformLocation(const std::string& name);
        GraphicsObjectID mShaderProgramID;

    private:
    };

} //end of namespace Renderer
} //end of namespace
#endif // SHADERPROGRAM_H
