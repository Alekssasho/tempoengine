#include "texture.h"

#include <SDL2/SDL_image.h>

namespace TempoEngine
{
namespace Renderer
{
    const std::string Texture2D::resType = "Texture2D";

    Texture2D::Texture2D()
        : Resource()
        , mWidth(0)
        , mHeight(0)
        , mTextureID(0)
    {
    }

    Texture2D::~Texture2D()
    {
        if(mTextureID)
            TEMPO_GL_CHECK_ERROR(glDeleteTextures(1, &mTextureID));
    }

    void Texture2D::Bind(GraphicsObjectEnum textureUnit)
    {
        TEMPO_GL_CHECK_ERROR(glActiveTexture(textureUnit));
        TEMPO_GL_CHECK_ERROR(glBindTexture(GL_TEXTURE_2D, mTextureID));
    }

    ResourcePtr Texture2DLoader::VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize)
    {
        SDL_RWops* rw = SDL_RWFromConstMem(rawBuffer.get(), rawSize);

        SDL_Surface* image = IMG_Load_RW(rw, 1);
        if(!image) {
            LOG(ERROR) << "Error while loading texture. With error: " << SDL_GetError();
            return ResourcePtr();
        }

        //ABGR because of little-endian, glTexImage2d read byte per byte; IF wana use RGBA, glTexImage2d must use GL_UNSIGNED_INT_8_8_8_8 in order to read the propper data
        SDL_Surface* convertedImage = SDL_ConvertSurfaceFormat(image, SDL_PIXELFORMAT_ABGR8888, 0);
        if(!convertedImage) {
            SDL_FreeSurface(image);
            LOG(ERROR) << "Error while converting texture. With error: " << SDL_GetError();
            return ResourcePtr();
        }

        std::shared_ptr<Texture2D> texture = std::make_shared<Texture2D>();

        texture->mWidth = convertedImage->w;
        texture->mHeight = convertedImage->h;

        GLenum target = GL_TEXTURE_2D;

        TEMPO_GL_CHECK_ERROR(glGenTextures(1, &texture->mTextureID));
        TEMPO_GL_CHECK_ERROR(glBindTexture(target, texture->mTextureID));

        TEMPO_GL_CHECK_ERROR(glTexImage2D(target,
                                          0,
                                          GL_RGBA,
                                          convertedImage->w,
                                          convertedImage->h,
                                          0,
                                          GL_RGBA,
                                          GL_UNSIGNED_BYTE,
                                          convertedImage->pixels));

        TEMPO_GL_CHECK_ERROR(glTexParameterf(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
        TEMPO_GL_CHECK_ERROR(glTexParameterf(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

        SDL_FreeSurface(convertedImage);
        SDL_FreeSurface(image);

        return texture;
    }
} //end of namespace Renderer
} //end of namespace TempoEngine
