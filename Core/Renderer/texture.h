#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include "../Platform.h"
#include "../platform_graphics.h"
#include "../Resource.h"

namespace TempoEngine
{
namespace Renderer
{

    class Texture2D : public Resource
    {
    public:
        Texture2D();
        virtual ~Texture2D() override;

        void Bind(GraphicsObjectEnum textureUnit);

        U32 Width() const { return mWidth; }
        U32 Height() const { return mHeight; }

        static const std::string resType;

    private:
        U32 mWidth;
        U32 mHeight;
        GraphicsObjectID mTextureID;

        friend class Texture2DLoader;
    };

    class Texture2DLoader : public IResourceLoader
    {
    public:
        std::string VGetResType() override { return Texture2D::resType; }
        ResourcePtr VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize) override;
    };
} //end of namespace Renderer
} //end of namespace TempoEngine

#endif
