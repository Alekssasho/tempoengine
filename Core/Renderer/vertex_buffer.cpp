#include "vertex_buffer.h"
#include "../Platform.h"

namespace TempoEngine
{
HardwareVertexBuffer::HardwareVertexBuffer(size_t vertexSize, size_t numVertices, BufferUsage usage)
    : mID(0)
    , mVertexSize(vertexSize)
    , mNumVertices(numVertices)
    , mUsage(usage)
{
    TEMPO_GL_CHECK_ERROR(glGenBuffers(1, &mID));
    if(!mID) {
        LOG(FATAL) << "Failed to create VBO ";
        return;
    }
    TEMPO_GL_CHECK_ERROR(glBindBuffer(GL_ARRAY_BUFFER, mID));
    TEMPO_GL_CHECK_ERROR(glBufferData(GL_ARRAY_BUFFER, mVertexSize * mNumVertices, NULL, GetGLBufferUsage(mUsage)));
}

HardwareVertexBuffer::~HardwareVertexBuffer()
{
    TEMPO_GL_CHECK_ERROR(glDeleteBuffers(1, &mID));
}

void HardwareVertexBuffer::WriteData(const void* source)
{
    TEMPO_GL_CHECK_ERROR(glBindBuffer(GL_ARRAY_BUFFER, mID));
    TEMPO_GL_CHECK_ERROR(glBufferData(GL_ARRAY_BUFFER, mVertexSize * mNumVertices, source, GetGLBufferUsage(mUsage)));
}

} //end of namespace
