#ifndef VERTEX_BUFFER_H
#define VERTEX_BUFFER_H

#include "../platform_graphics.h"

namespace TempoEngine
{
class HardwareVertexBuffer
{
public:
    HardwareVertexBuffer(size_t vertexSize, size_t numVertices, BufferUsage usage);
    ~HardwareVertexBuffer();

    void WriteData(const void* source);

private:
    GraphicsObjectID mID;
    size_t mVertexSize;
    size_t mNumVertices;
    BufferUsage mUsage;
};
} // end of namespace

#endif