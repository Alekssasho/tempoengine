#include "vertexarray.h"

#include "../Platform.h"

namespace TempoEngine
{

void HardwareVertexArray::Unbind()
{
    TEMPO_GL_CHECK_ERROR(glBindVertexArray(0));
}

HardwareVertexArray::HardwareVertexArray()
{
    TEMPO_GL_CHECK_ERROR(glGenVertexArrays(1, &mID));
}

HardwareVertexArray::~HardwareVertexArray()
{
    TEMPO_GL_CHECK_ERROR(glDeleteVertexArrays(1, &mID));
}

void HardwareVertexArray::Bind()
{
    TEMPO_GL_CHECK_ERROR(glBindVertexArray(mID));
}

} //end of namespace
