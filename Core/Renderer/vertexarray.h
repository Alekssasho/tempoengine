#ifndef VERTEX_ARRAY_H
#define VERTEX_ARRAY_H

#include "../platform_graphics.h"

namespace TempoEngine
{
class HardwareVertexArray
{
public:
    HardwareVertexArray();
    ~HardwareVertexArray();

    void Bind();

    static void Unbind();

private:
    GraphicsObjectID mID;
};
} //end of namespace
#endif