//
//  Resource.h
//  Engine
//
//  Created by Aleksandar Angelov on 6/30/14.
//
//

#ifndef __Engine__Resource__
#define __Engine__Resource__

#include "Interfaces.h"
#include "Utilities.h"

namespace TempoEngine
{
//Every resource which needs to be loaded need to inherit from this class,
//and also include static const string with name resType which will be the unique name of the type of resource
class Resource : public Utility::Noncopyable
{
private:
    friend class ResourceManager;

    ResourceIdentifier mName;
    void SetName(const ResourceIdentifier& name) { mName = name; }

public:
    virtual ~Resource() {}

    const ResourceIdentifier& GetName() const { return mName; }
};
} //end of namespace

#endif /* defined(__Engine__Resource__) */
