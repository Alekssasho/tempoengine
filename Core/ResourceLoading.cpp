#include "ResourceLoading.h"

#include <fstream>
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>

namespace TempoEngine
{
    
boost::filesystem::path Path::mWorkingDir = "";
    
ResourceManager::ResourceManager()
{
}

ResourceManager::~ResourceManager()
{
    //stl containers handle themselfs
}

ResourcePtr ResourceManager::Find(const ResourceIdentifier& r)
{
    auto findIt = mResources.find(r);
    if(findIt != mResources.end()) {
        return findIt->second;
    }

    return ResourcePtr();
}

ResourcePtr ResourceManager::Load(const ResourceIdentifier& resName, const std::string& resType)
{
    auto findIt = mLoaders.find(resType);
    if(findIt == mLoaders.end()) {
        LOG(ERROR) << "ResourceManager: Doesnt have loader for type " << resType;
        return ResourcePtr();
    }

    const auto& loader = findIt->second;

    int rawSize = mFile->VGetRawResourceSize(resName);
    if(rawSize == -1) {
        LOG(ERROR) << "ResourceManager: Didn't find " << resName;
        return ResourcePtr();
    }

    ByteBuffer rawBuffer = std::make_unique<char[]>(rawSize);

    mFile->VGetRawResource(resName, rawBuffer);

    ResourcePtr res = loader->VLoadResource(rawBuffer, rawSize);
    SDL_assert(res && "Loader coudn't load the requested resource");
    res->SetName(resName);

    if(res)
        mResources[resName] = res;

    return res;
}

void ResourceManager::UnloadResource(const ResourceIdentifier& gonner)
{
    mResources.erase(gonner);
}

bool ResourceManager::Init(IResourceFile* resFile)
{
    mFile.reset(resFile);
    return mFile->VOpen();
}

void ResourceManager::RegisterLoader(std::unique_ptr<IResourceLoader> loader)
{
    mLoaders[loader->VGetResType()] = std::move(loader);
}

void ResourceManager::UnloadAll()
{
    mResources.clear();
}

//This is std::erase(std::remove_if()) idiom on map, which doesnt support std::remove
void ResourceManager::CheckNotUsedResources()
{
    for(auto it = mResources.begin(); it != mResources.end();) {
        if(it->second.unique()) {
            it = mResources.erase(it);
        } else {
            ++it;
        }
    }
}

DevelopmentResourceFile::DevelopmentResourceFile(const std::string& assetDir)
    : mAssetDir(assetDir)
    , mNumOfFiles(0)
{
}

DevelopmentResourceFile::~DevelopmentResourceFile()
{
}

bool DevelopmentResourceFile::VOpen()
{
    this->ReadAssetsDirectory(mAssetDir.String());
    return true;
}

int DevelopmentResourceFile::VGetRawResourceSize(const ResourceIdentifier& resName)
{
    if(!boost::filesystem::exists(mAssetDir.String() + resName))
        return -1;
    return static_cast<int>(boost::filesystem::file_size(mAssetDir.String() + resName));
}

int DevelopmentResourceFile::VGetRawResource(const ResourceIdentifier& resName, ByteBuffer& buffer)
{
    int size = this->VGetRawResourceSize(resName);
    if(size == -1) {
        LOG(ERROR) << "Not existing resource";
        return -1;
    }

    std::ifstream file(mAssetDir.String() + resName, std::ios::binary | std::ios::in);
    if(file.is_open()) {
        file.read(buffer.get(), size);
        file.close();
        return static_cast<int>(file.gcount());
    }

    LOG(ERROR) << "Couldn't open file";
    return -1;
}

int DevelopmentResourceFile::VGetNumResources() const
{
    return mNumOfFiles;
}

void DevelopmentResourceFile::ReadAssetsDirectory(const std::string& dir)
{
    using namespace boost::filesystem;

    path dirPath(dir);
    std::for_each(directory_iterator(dirPath), directory_iterator(), [this](const directory_entry& entry) {
            if(is_regular_file(entry)) {
                ++mNumOfFiles;
            } else if(is_directory(entry)) {
                this->ReadAssetsDirectory(entry.path().string());
            }
    });
}

} //end of namespace