#ifndef RESOURCE_LOADING_H_
#define RESOURCE_LOADING_H_

#include "Interfaces.h"

#include "Utilities.h"
#include "Resource.h"
#include <unordered_map>
#include <functional>

#include <boost/filesystem/path.hpp>

namespace TempoEngine
{
    
class Path
{
public:
    Path(std::string path)
    : mPath(std::move(path))
    {
    }
    
    std::string String() const
    {
        return (mWorkingDir / mPath).generic_string();
    }
    
private:
    boost::filesystem::path mPath;
    static boost::filesystem::path mWorkingDir;
    
    static void SetWorkingDir(const boost::filesystem::path& dir) { mWorkingDir = dir; }
    friend int TempoEngineStart(int argc, char* argv[]);
};
    
class ResourceManager
{
    using ResourceMap = std::unordered_map<ResourceIdentifier, ResourcePtr>;
    using ResourceLoaders = std::unordered_map<std::string, std::unique_ptr<IResourceLoader>>;

private:
    ResourceMap mResources;
    ResourceLoaders mLoaders;

    std::unique_ptr<IResourceFile> mFile;

    ResourcePtr Find(const ResourceIdentifier& r);
    ResourcePtr Load(const ResourceIdentifier& r, const std::string& resType);

public:
    ResourceManager();
    ~ResourceManager();

    bool Init(IResourceFile* resFile);
    void RegisterLoader(std::unique_ptr<IResourceLoader> loader);

    template <typename T>
    std::shared_ptr<T> GetResource(const ResourceIdentifier& r);

    void UnloadResource(const ResourceIdentifier& r);
    void UnloadAll();
    void CheckNotUsedResources();
};

class DevelopmentResourceFile : public IResourceFile
{
    Path mAssetDir;
    U32 mNumOfFiles;

public:
    explicit DevelopmentResourceFile(const std::string& assetDir);
    virtual ~DevelopmentResourceFile() override;

    virtual bool VOpen() override;
    virtual int VGetRawResourceSize(const ResourceIdentifier& r) override;
    virtual int VGetRawResource(const ResourceIdentifier& r, ByteBuffer& buffer) override;
    virtual int VGetNumResources() const override;

private:
    void ReadAssetsDirectory(const std::string& dir);
};

template <typename T>
std::shared_ptr<T> ResourceManager::GetResource(const ResourceIdentifier& r)
{
    ResourcePtr pRes = this->Find(r);
    if(!pRes) {
        pRes = this->Load(r, T::resType);

        if(!pRes)
            return std::shared_ptr<T>();
    }

    return std::static_pointer_cast<T>(pRes);
}

} //end of namespace
#endif