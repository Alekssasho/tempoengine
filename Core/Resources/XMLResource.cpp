//
//  XMLResource.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 11/5/14.
//
//

#include "XMLResource.h"

//needed for using rapidxml compiled without exceptions
void rapidxml::parse_error_handler(const char* what, void* where)
{
    LOG(FATAL) << "Rapidxml failed with error " << what;
}

namespace TempoEngine
{
    const std::string XMLResource::resType("XML");
    
    ResourcePtr XMLResourceLoader::VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize)
    {
        auto result = std::make_shared<XMLResource>();
        
        result->mResource = std::make_unique<char[]>(rawSize + 1);
        memcpy(result->mResource.get(), rawBuffer.get(), rawSize);
        result->mResource.get()[rawSize] = '\0';
        result->doc.parse<0>(result->mResource.get());
        
        return result;
    }
    
}//end of namespace