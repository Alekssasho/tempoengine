//
//  XMLResource.h
//  Engine
//
//  Created by Aleksandar Angelov on 11/5/14.
//
//

#ifndef __Engine__XMLResource__
#define __Engine__XMLResource__

#include "../Resource.h"

#define RAPIDXML_NO_EXCEPTIONS
#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_utils.hpp>

namespace TempoEngine
{
    class XMLResource : public Resource
    {
    public:
        static const std::string resType;
        
        rapidxml::xml_document<> doc;
    private:
        friend class XMLResourceLoader;
        
        ByteBuffer mResource;
    };
    
    class XMLResourceLoader : public IResourceLoader
    {
    public:
        std::string VGetResType() override { return XMLResource::resType; }
        ResourcePtr VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize) override;
    };
    
}//end of namespace

#endif /* defined(__Engine__XMLResource__) */
