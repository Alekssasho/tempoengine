//
//  ScriptEvent.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 10/31/14.
//
//

#include "ScriptEvent.h"

namespace TempoEngine
{
    
    ScriptEventListener::ScriptEventListener(const luabind::object& luacallback, const EventType& type)
    : mLuaCallback(luacallback), mType(type)
    {
        EventManager::Get()->AddListener(fastdelegate::MakeDelegate(this, &ScriptEventListener::ScriptEventDelegate), mType);
    }
    
    ScriptEventListener::~ScriptEventListener()
    {
        EventManager::Get()->RemoveListener(fastdelegate::MakeDelegate(this, &ScriptEventListener::ScriptEventDelegate), mType);
    }
    
    void ScriptEventListener::ScriptEventDelegate(IEventDataPtr pEventPtr)
    {
        auto pScriptEvent = static_cast<ScriptEvent*>(pEventPtr.get());
        mLuaCallback(pScriptEvent);
    }
    
    U32 ScriptEventListenersManager::AddListener(ScriptEventListenerPtr listener)
    {
        mListeners[++mCurrentIndex] = std::move(listener);
        return mCurrentIndex;
    }
    
    void ScriptEventListenersManager::RemoveListener(U32 index)
    {
        mListeners.erase(index);
    }
    
}//end of namespace