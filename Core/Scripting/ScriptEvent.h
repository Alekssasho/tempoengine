//
//  ScriptEvent.h
//  Engine
//
//  Created by Aleksandar Angelov on 10/31/14.
//
//

#ifndef __Engine__ScriptEvent__
#define __Engine__ScriptEvent__

#include "../EventManager/EventManager.h"
#include "../platform.h"
#include <unordered_map>

#include <lua5.1/lua.hpp>
#include <luabind/class.hpp>

namespace TempoEngine
{
    //Every derived class of ScriptEvent must define static function named RegisterScriptEvent() which returns luabind::scope
    class ScriptEvent : public BaseEventData, public luabind::wrap_base
    {
    public:
    private:
    };
    
    class ScriptEventListener
    {
    public:
        ScriptEventListener(const luabind::object& luacallback, const EventType& type);
        ~ScriptEventListener();
        
        void ScriptEventDelegate(IEventDataPtr pEventPtr);
    private:
        luabind::object mLuaCallback;
        EventType mType;
    };
    
    class ScriptEventListenersManager
    {
        using ScriptEventListenerPtr = std::unique_ptr<ScriptEventListener>;
    public:
        U32 AddListener(ScriptEventListenerPtr list);
        void RemoveListener(U32 index);
    private:
        std::unordered_map<U32, ScriptEventListenerPtr> mListeners;
        
        U32 mCurrentIndex = 0;
    };
    
}//end of namespace

#endif /* defined(__Engine__ScriptEvent__) */
