//
//  ScriptExports.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 10/22/14.
//
//

#include "ScriptExports.h"
#include "ScriptStateManager.h"
#include "ScriptProcess.h"
#include "ScriptEvent.h"
#include "../game.h"
#include <lua5.1/lua.hpp>

#include <luabind/luabind.hpp>
#include <luabind/adopt_policy.hpp>

namespace TempoEngine
{
    
    static void ScriptLog(const char* str)
    {
        LOG(INFO) << "Lua: " << str;
    }
    
    static bool ExecuteFile(const char* str)
    {
        return ScriptStateManager::Instance()->VExecuteFile(ResourceIdentifier(str));
    }
    
    static void AttachScriptProcess(ScriptProcess* scriptProc)
    {
        gGame->GetGameLogic()->AttachProcess(StrongProcessPtr(scriptProc));
    }
    
    static void QueueEvent(ScriptEvent* scriptEvent)
    {
        EventManager::Get()->QueueEvent(IEventDataPtr(scriptEvent));
    }
    
    static void TriggerEvent(ScriptEvent* scriptEvent)
    {
        EventManager::Get()->TriggerEvent(IEventDataPtr(scriptEvent));
    }
    
    static ScriptEventListenersManager* sListenerManager;
    
    static U32 RegisterEventListener(EventType eventType, luabind::object callback)
    {
        return sListenerManager->AddListener(std::make_unique<ScriptEventListener>(callback, eventType));
    }
    
    static ActorId CreateActor(const char* str)
    {
        auto res = gGame->GetGameLogic()->VCreateActor(ResourceIdentifier(str));
        return res->GetId();
    }
 
    void ScriptExports::Register()
    {
        auto pState = ScriptStateManager::Instance()->GetState();
        
        sListenerManager = new ScriptEventListenersManager;
        
        using namespace luabind;
        open(pState);
        
        module(pState, "Engine")
        [
            def("Log", ScriptLog),
            def("ExecuteFile", ExecuteFile),
            def("AttachProcess", AttachScriptProcess, adopt(_1)),
            def("QueueEvent", QueueEvent, adopt(_1)),
            def("TriggerEvent", TriggerEvent, adopt(_1)),
            def("RegisterEventDelegate", RegisterEventListener),
            def("CreateActor", CreateActor),
            ScriptProcess::RegisterClass()
         ];
    }
    
    void ScriptExports::Unregister()
    {
        delete sListenerManager;
    }
    
}//end of namespace