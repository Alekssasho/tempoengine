//
//  ScriptExports.h
//  Engine
//
//  Created by Aleksandar Angelov on 10/22/14.
//
//

#ifndef __Engine__ScriptExports__
#define __Engine__ScriptExports__

namespace TempoEngine
{
    namespace ScriptExports
    {
        void Register();
        void Unregister();
    }
}

#endif /* defined(__Engine__ScriptExports__) */
