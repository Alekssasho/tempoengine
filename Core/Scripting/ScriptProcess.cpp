//
//  ScriptProcess.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 10/27/14.
//
//

#include "ScriptProcess.h"

#include <luabind/adopt_policy.hpp>

namespace TempoEngine
{
    ScriptProcess::ScriptProcess()
    {
    }
    
    ScriptProcess::~ScriptProcess()
    {
    }
    
    void ScriptProcess::VOnInit()
    {
        this->Process::VOnInit();
        this->call<void>("OnInit");
    }
    
    void ScriptProcess::VOnUpdate(U32 deltaMs)
    {
        this->call<void>("OnUpdate", deltaMs);
    }
    
    void ScriptProcess::VOnSuccess()
    {
        this->call<void>("OnSuccess");
    }
    
    void ScriptProcess::VOnFail()
    {
        this->call<void>("OnFail");
    }
    
    void ScriptProcess::VOnAbort()
    {
        this->call<void>("OnAbort");
    }
    
    void ScriptProcess::DefaultOnUpdate(Process* ptr, U32 deltaMs)
    {
        LOG(FATAL) << "ScriptProcess: OnUpdate not overriden from lua.";
    }
    
    void ScriptProcess::DefaultOnInit(Process* ptr)
    {
        return ptr->Process::VOnInit();
    }
    
    void ScriptProcess::DefaultOn(Process* ptr)
    {
        return;
    }
    
    void ScriptProcess::ScriptAttachChild(ScriptProcess* scriptProc)
    {
        this->AttachChild(StrongProcessPtr(scriptProc));
    }
    
    luabind::scope ScriptProcess::RegisterClass()
    {
        using namespace luabind;
        return class_<Process, ScriptProcess>("Process")
        .def(constructor<>())
        .def("Succeed", &ScriptProcess::Succeed)
        .def("Fail", &ScriptProcess::Fail)
        .def("Pause", &ScriptProcess::Pause)
        .def("UnPause", &ScriptProcess::UnPause)
        .def("IsAlive", &ScriptProcess::IsAlive)
        .def("IsDead", &ScriptProcess::IsDead)
        .def("IsPaused", &ScriptProcess::IsPaused)
        .def("OnInit", &ScriptProcess::VOnInit, &ScriptProcess::DefaultOnInit)
        .def("OnSuccess", &ScriptProcess::VOnSuccess, &ScriptProcess::DefaultOn)
        .def("OnFail", &ScriptProcess::VOnFail, &ScriptProcess::DefaultOn)
        .def("OnAbort", &ScriptProcess::VOnAbort, &ScriptProcess::DefaultOn)
        .def("OnUpdate", &ScriptProcess::VOnUpdate, &ScriptProcess::DefaultOnUpdate)
        .def("AttachChild", &ScriptProcess::ScriptAttachChild, adopt(_2));
    }
}//end of namespace