//
//  ScriptProcess.h
//  Engine
//
//  Created by Aleksandar Angelov on 10/27/14.
//
//

#ifndef __Engine__ScriptProcess__
#define __Engine__ScriptProcess__

#include "../Process.h"

#include <lua5.1/lua.hpp>
#include <luabind/luabind.hpp>

namespace TempoEngine
{
    
    class ScriptProcess : public Process, public luabind::wrap_base
    {
    public:
        ScriptProcess();
        ~ScriptProcess();
        
        static luabind::scope RegisterClass();
    private:
        void VOnInit() override;
        void VOnUpdate(U32 deltaMs) override;
        void VOnSuccess() override;
        void VOnFail() override;
        void VOnAbort() override;
        
        static void DefaultOnUpdate(Process* ptr, U32 deltaMs);
        static void DefaultOnInit(Process* ptr);
        static void DefaultOn(Process* ptr);
        
        void ScriptAttachChild(ScriptProcess* scriptProc);
    };
    
}//end of namespace

#endif /* defined(__Engine__ScriptProcess__) */
