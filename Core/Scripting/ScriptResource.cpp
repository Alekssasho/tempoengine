//
//  ScriptResource.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 10/22/14.
//
//

#include "ScriptResource.h"
#include "ScriptStateManager.h"

namespace TempoEngine
{
    
    const std::string ScriptResource::resType = "LuaScript";
    
    ResourcePtr ScriptResourceLoader::VLoadResource(const ByteBuffer &rawBuffer, U32 rawSize)
    {
        std::string str(rawBuffer.get(), rawBuffer.get() + rawSize);
        ScriptStateManager::Instance()->VExecuteString(str.c_str());
        return std::make_shared<ScriptResource>();
    }
    
}//end of namespace