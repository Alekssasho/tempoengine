//
//  ScriptResource.h
//  Engine
//
//  Created by Aleksandar Angelov on 10/22/14.
//
//

#ifndef __Engine__ScriptResource__
#define __Engine__ScriptResource__

#include "../Resource.h"

namespace TempoEngine
{
    
    class ScriptResource : public Resource
    {
    public:
        static const std::string resType;
    };
    
    class ScriptResourceLoader : public IResourceLoader
    {
    public:
        std::string VGetResType() override { return ScriptResource::resType; }
        ResourcePtr VLoadResource(const ByteBuffer& rawBuffer, U32 rawSize) override;
    };
    
}//end of namespace

#endif /* defined(__Engine__ScriptResource__) */
