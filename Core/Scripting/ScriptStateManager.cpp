//
//  ScriptStateManager.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 10/20/14.
//
//

#include <lua5.1/lua.hpp>
#include "ScriptStateManager.h"
#include "ScriptResource.h"
#include "../game.h"

namespace TempoEngine
{
    bool ScriptStateManager::VInit()
    {
        mState = luaL_newstate();
        luaL_openlibs(mState);
        
        return true;
    }
    
    bool ScriptStateManager::VExecuteFile(const ResourceIdentifier &res)
    {
        auto r = gGame->GetResourceManager().GetResource<ScriptResource>(res);
        if(r) return true;
        return false;
    }
    
    void ScriptStateManager::VExecuteString(const char *str)
    {
        assert(strlen(str) && "Zero length string in ExecuteString");
        luaL_dostring(mState, str);
    }
    
    ScriptStateManager::~ScriptStateManager()
    {
        lua_close(mState);
        mState = nullptr;
    }
    
}//end of namespace