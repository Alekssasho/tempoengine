//
//  ScriptStateManager.h
//  Engine
//
//  Created by Aleksandar Angelov on 10/20/14.
//
//

#ifndef __Engine__ScriptStateManager__
#define __Engine__ScriptStateManager__

#include "../Utilities.h"
#include "../Interfaces.h"

struct lua_State;

namespace TempoEngine
{
    class ScriptStateManager : public Utility::Singleton<ScriptStateManager>, IScriptManager
    {
    public:
        ~ScriptStateManager();
        
        bool VInit() override;
        bool VExecuteFile(const ResourceIdentifier& res) override;
        void VExecuteString(const char* str) override;
        
        lua_State* GetState() const { return mState; }
    private:
        lua_State* mState = nullptr;
    };
}//end of namespace

#endif /* defined(__Engine__ScriptStateManager__) */
