//
//  TempoMath.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 5/27/14.
//
//

#include "TempoMath.h"
#include "Platform.h"

namespace TempoEngine
{

void Transform::CalculateMatrix()
{
    mMatrix = glm::translate(mPosition) * glm::yawPitchRoll(mRotation.x, mRotation.y, mRotation.z) * glm::scale(mScale);
}
    
Degree::Degree(Real value)
    : mValue(value)
{
}

Degree::Degree(const Radian& rad)
    : mValue(rad.ValueDegree())
{
}

Real Degree::ValueRadians() const
{
    return glm::radians(mValue);
}

Radian::Radian(Real value)
    : mValue(value)
{
}

Radian::Radian(const Degree& deg)
    : mValue(deg.ValueRadians())
{
}

Real Radian::ValueDegree() const
{
    return glm::degrees(mValue);
}

inline bool Plane::Inside(const Vector3& point) const
{
    return (glm::dot(mCoords.xyz(), point) + mCoords.w) >= 0.0f;
}

inline bool Plane::Inside(const Vector3& point, const Real radius) const
{
    return (glm::dot(mCoords.xyz(), point) + mCoords.w) >= -radius;
}

Frustum::Frustum()
    : mFov(Radian(glm::quarter_pi<Real>()))
    , mAspect(1.0f)
    , mNear(1.0f)
    , mFar(1000.0f)
{
}

bool Frustum::Inside(const Vector3& point) const
{
    for(const auto& plane : mPlanes) {
        if(!plane.Inside(point)) {
            return false;
        }
    }
    return true;
}

bool Frustum::Inside(const Vector3& point, Real radius) const
{
    for(const auto& plane : mPlanes) {
        if(!plane.Inside(point, radius)) {
            return false;
        }
    }
    return true;
}

void Frustum::Recalculate()
{
    float tanFovOver2 = glm::tan(mFov.ValueRadians() / 2.0f);

    Vector3 nearRight = (mNear * tanFovOver2) * mAspect * gRight;
    Vector3 farRight = (mFar * tanFovOver2) * mAspect * gRight;
    Vector3 nearUp = (mNear * tanFovOver2) * gUp;
    Vector3 farUp = (mFar * tanFovOver2) * gUp;

    mNearClip[0] = (mNear * gForward) - nearRight + nearUp;
    mNearClip[1] = (mNear * gForward) + nearRight + nearUp;
    mNearClip[2] = (mNear * gForward) + nearRight - nearUp;
    mNearClip[3] = (mNear * gForward) - nearRight - nearUp;

    mFarClip[0] = (mFar * gForward) - farRight + farUp;
    mFarClip[1] = (mFar * gForward) + farRight + farUp;
    mFarClip[2] = (mFar * gForward) + farRight - farUp;
    mFarClip[3] = (mFar * gForward) - farRight - farUp;

    //Do not shuffle arguments because they obey CCW winding in Right handed system
    Vector3 origin(0.0f, 0.0f, 0.0f);
    mPlanes[Near].Init(mNearClip[0], mNearClip[1], mNearClip[2]);
    mPlanes[Far].Init(mFarClip[2], mFarClip[1], mFarClip[0]);
    mPlanes[Right].Init(origin, mFarClip[1], mFarClip[2]);
    mPlanes[Top].Init(origin, mFarClip[0], mFarClip[1]);
    mPlanes[Left].Init(origin, mFarClip[3], mFarClip[0]);
    mPlanes[Bottom].Init(origin, mFarClip[2], mFarClip[3]);
}

Vector3 GetTranslation(const Matrix4& mat)
{
    return Vector3(mat[3]);
}

void SetTranslation(Matrix4& mat, const Vector3& translation)
{
    mat[3] = Vector4(translation, 1.0f);
}

Vector3 GetDirection(const Matrix4& mat)
{
    return Vector3(mat * Vector4(gForward, 0.0f));
}

} //end of namespace