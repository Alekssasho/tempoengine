#ifndef TEMPO_MATH_H
#define TEMPO_MATH_H

#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>

#include <array>
#include <stack>

namespace TempoEngine
{

typedef glm::vec2 Vector2;
typedef glm::vec3 Vector3;
typedef glm::vec4 Vector4;

typedef glm::mat4 Matrix4;

typedef glm::ivec2 iVector2;

struct Rect
{
    iVector2 topLeft;
    iVector2 size;
};

const Matrix4 gIdentity(1.0f);

const Vector3 gUp(0.0f, 1.0f, 0.0f);
const Vector3 gRight(1.0f, 0.0f, 0.0f);
const Vector3 gForward(0.0f, 0.0f, -1.0f); // -1 because of Right Handed System used in the engine

const Vector3 gZero(0.0f, 0.0f, 0.0f);
const Vector3 gOnes(1.0f, 1.0f, 1.0f);

//Matrix manipulation helper functions. Need better design, maybe enacpsulate in class
Vector3 GetTranslation(const Matrix4& mat);
void SetTranslation(Matrix4& mat, const Vector3& translation);
Vector3 GetDirection(const Matrix4& mat);

    
using Real = float;
    
class Transform
{
public:
    Transform()
    :Transform(gZero, gZero, gOnes)
    {}
    
    Transform(const Vector3& p, const Vector3& r, const Vector3& s)
    : mPosition(p), mRotation(r), mScale(s)
    {
        this->CalculateMatrix();
    }
    
    const Vector3& Position() const { return mPosition; }
    const Vector3& Rotation() const { return mRotation; }
    const Vector3& Scale() const { return mScale; }
    const Matrix4& Matrix() const { return mMatrix; }
    
    void SetPosition(const Vector3& pos) { mPosition = pos; this->CalculateMatrix(); }
    void SetRotation(const Vector3& rot) { mRotation = rot; this->CalculateMatrix(); }
    void SetScale(const Vector3& sc) { mScale = sc; this->CalculateMatrix(); }
    
private:
    Vector3 mPosition;
    Vector3 mRotation;
    Vector3 mScale;
    Matrix4 mMatrix;
    void CalculateMatrix();
};
    
const Transform gDefaultTransform;

class Radian;
class Degree
{
public:
    explicit Degree(Real value = 0.0f);
    Degree(const Radian& rad);

    Real ValueDegree() const { return mValue; }
    Real ValueRadians() const;

    //TODO: implement all kind of math operations

private:
    Real mValue;
};

class Radian
{
public:
    explicit Radian(Real value = 0.0f);
    Radian(const Degree& deg);

    Real ValueDegree() const;
    Real ValueRadians() const { return mValue; }

private:
    Real mValue;
};

//Plane has equation A*x + B*y + C*z + D = 0
class Plane
{
public:
    inline void Normalize();

    inline void Init(const Vector3& p0, const Vector3& p1, const Vector3& p2);

    bool Inside(const Vector3& point) const;
    bool Inside(const Vector3& point, const float radius) const;

private:
    Vector4 mCoords;
};

using std::array;

class Frustum
{
public:
    enum Side { Near,
                Far,
                Top,
                Right,
                Bottom,
                Left };

    array<Plane, 6> mPlanes;
    array<Vector3, 4> mNearClip;
    array<Vector3, 4> mFarClip;

    Radian mFov;
    Real mAspect;
    Real mNear;
    Real mFar;

    Frustum();

    bool Inside(const Vector3& point) const;
    bool Inside(const Vector3& point, Real radius) const;

    const Plane& Get(Side side) { return mPlanes[side]; }
    void SetFov(Radian fov)
    {
        mFov = fov;
        this->Recalculate();
    }
    void SetAspect(Real aspect)
    {
        mAspect = aspect;
        this->Recalculate();
    }
    void SetNear(Real nearClip)
    {
        mNear = nearClip;
        this->Recalculate();
    }
    void SetFar(Real farClip)
    {
        mFar = farClip;
        this->Recalculate();
    }
    void Init(const Radian fov, const Real aspect, const Real near, const Real far)
    {
        mFov = fov;
        mAspect = aspect;
        mNear = near;
        mFar = far;
        this->Recalculate();
    }

private:
    void Recalculate();
};

class MatrixStack
{
public:
    MatrixStack()
    {
        mStack.push(Matrix4{ 1.0f });
    }

    void Push()
    {
        mStack.push(mStack.top());
    }

    void Pop()
    {
        if(mStack.size() > 1)
            mStack.pop();
    }

    void MultiplyTopMatrix(const Matrix4& mat)
    {
        mStack.top() *= mat;
    }

    Matrix4& GetTopMatrix()
    {
        return mStack.top();
    }

private:
    std::stack<Matrix4> mStack;
};

//Implementations

inline void Plane::Normalize()
{
    float mag = glm::length(mCoords.xyz());
    mCoords /= mag;
}

inline void Plane::Init(const Vector3& p0, const Vector3& p1, const Vector3& p2)
{
    auto edge1 = p1 - p0;
    auto edge2 = p2 - p0;
    mCoords = glm::normalize(glm::cross(edge1, edge2)).xyzx(); //last one is redundant
    mCoords.w = -glm::dot(mCoords.xyz(), p0);
}

} //end of namespace

#endif
