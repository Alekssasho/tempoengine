//
//  Utilities.h
//  Engine
//
//  Created by Aleksandar Angelov on 7/2/14.
//
//

#ifndef Engine_Utilities_h
#define Engine_Utilities_h

#include <utility>
#include <boost/noncopyable.hpp>

namespace TempoEngine
{
namespace Utility
{

    using Noncopyable = boost::noncopyable;

    template <typename T>
    class Singleton : public Noncopyable
    {
    public:
        static T* Instance()
        {
            return mInstance;
        }

        template <typename... Args>
        static bool CreateInstance(Args... args)
        {
            if(!mInstance) {
                mInstance = new T(std::forward<Args>(args)...);
                return true;
            }

            //TODO: we already have instance report error
            return false;
        }

        static bool DeleteInstance()
        {
            if(mInstance) {
                delete mInstance;
                mInstance = nullptr;
                return true;
            }

            return false;
        }

    private:
        static T* mInstance;
    };

    template <typename T>
    T* Singleton<T>::mInstance = nullptr;

} //end of namspace Utility
} //end of namespace TempoEngine

#endif
