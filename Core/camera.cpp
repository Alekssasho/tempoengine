#include "camera.h"
#include "Platform.h"
#include "game.h"

namespace TempoEngine
{

const static int MARGIN = 10;

Camera::Camera()
    : mPosition(0.0f)
    , mTarget(0.0f, 0.0f, 1.0f)
    , mUp(0.0f, 1.0f, 0.0f)
    , mProjectionMatrix(1.0f)
{
    this->Init();
}

Camera::Camera(const Vector3& position, const Vector3& target, const Vector3& up)
    : mPosition(position)
    , mTarget(glm::normalize(target))
    , mUp(glm::normalize(up))
    , mProjectionMatrix(1.0f)
{
    this->Init();
    this->ComputeViewMatrix();
}

void Camera::Init()
{
    Vector3 hTarget(glm::normalize(Vector3(mTarget.x, 0.0f, mTarget.z)));

    if(hTarget.z >= 0.0f) {
        if(hTarget.x >= 0.0f) {
            mAngleH = 360.0f - glm::degrees(glm::asin(hTarget.z));
        } else {
            mAngleH = 180.0f + glm::degrees(glm::asin(hTarget.z));
        }
    } else {
        if(hTarget.x >= 0.0f) {
            mAngleH = glm::degrees(glm::asin(-hTarget.z));
        } else {
            mAngleH = 90.0f + glm::degrees(glm::asin(-hTarget.z));
        }
    }

    mAngleV = -glm::degrees(glm::asin(mTarget.y));
    mOnLeftEdge = false;
    mOnLowerEdge = false;
    mOnRightEdge = false;
    mOnUpperEdge = false;

    //    mMousePos = glm::ivec2(GameOld::Instance().ScreenWidth() / 2, GameOld::Instance().ScreenHeight() / 2);

    //    GameOld::Instance().GetInputHandler().SetMousePosition(mMousePos.x, mMousePos.y);
}

void Camera::SetPosition(const Vector3& position)
{
    mPosition = position;
    this->ComputeViewMatrix();
}

void Camera::SetTarget(const Vector3& target)
{
    mTarget = target;
    this->ComputeViewMatrix();
}

void Camera::SetUp(const Vector3& up)
{
    mUp = up;
    this->ComputeViewMatrix();
}

void Camera::ComputeViewMatrix()
{
    mViewMatrix = glm::lookAt(mPosition, mPosition + mTarget, mUp);
}

void Camera::MakePerspectiveProjection(const float fov, const float aspectRation, const float zNear, const float zFar)
{
    mProjectionMatrix = glm::perspective(glm::radians(fov), aspectRation, zNear, zFar);
}

const float Camera::sStepSize = 0.1f;

bool Camera::OnInputEvent(const InputEvent& event)
{
    if(event.type == InputEventType::KEYBOARD_DOWN_EVENT || event.type == InputEventType::KEYBOARD_UP_EVENT) {
        return this->OnKeyboard(event);
    } else {
        this->OnMouse(event);
        return true;
    }
}

bool Camera::OnKeyboard(const InputEvent& event)
{
    bool result = true;
    if(event.type == InputEventType::KEYBOARD_DOWN_EVENT) {
        switch(event.key.code) {
        case SDLK_UP:
            mPosition += (glm::normalize(mTarget) * sStepSize);
            this->ComputeViewMatrix();
            break;
        case SDLK_DOWN:
            mPosition -= (glm::normalize(mTarget) * sStepSize);
            this->ComputeViewMatrix();
            break;
        case SDLK_LEFT:
            mPosition += (glm::normalize(glm::cross(mUp, mTarget)) * sStepSize);
            this->ComputeViewMatrix();
            break;
        case SDLK_RIGHT:
            mPosition += (glm::normalize(glm::cross(mTarget, mUp)) * sStepSize);
            this->ComputeViewMatrix();
            break;
        default:
            result = false;
            break;
        }
    }
    return result;
}

void Camera::OnMouse(const InputEvent& event)
{
    int x = event.motion.x;
    int y = event.motion.y;
    const int deltaX = x - mMousePos.x;
    const int deltaY = y - mMousePos.y;

    mMousePos.x = x;
    mMousePos.y = y;

    mAngleH -= static_cast<float>(deltaX) / 20.0f;
    mAngleV += static_cast<float>(deltaY) / 20.0f;

    if(deltaX == 0) {
        if(x <= MARGIN) {
            mOnLeftEdge = true;
        } else if(x >= /*(GameOld::Instance().ScreenWidth() -*/ MARGIN /*)*/) {
            mOnRightEdge = true;
        }
    } else {
        mOnLeftEdge = false;
        mOnRightEdge = false;
    }

    if(deltaY == 0) {
        if(y <= MARGIN) {
            mOnUpperEdge = true;
        } else if(y >= /*(GameOld::Instance().ScreenHeight() - */ MARGIN /*)*/) {
            mOnLowerEdge = true;
        }
    } else {
        mOnUpperEdge = false;
        mOnLowerEdge = false;
    }

    this->UpdateState();
}

void Camera::Update()
{
    bool shouldUpdateState = false;

    if(mOnLeftEdge) {
        mAngleH += 0.1f;
        shouldUpdateState = true;
    } else if(mOnRightEdge) {
        mAngleH -= 0.1f;
        shouldUpdateState = true;
    }

    if(mOnUpperEdge) {
        if(mAngleV > -90.0f) {
            mAngleV -= 0.1f;
            shouldUpdateState = true;
        }
    } else if(mOnLowerEdge) {
        if(mAngleV < 90.0f) {
            mAngleV += 0.1f;
            shouldUpdateState = true;
        }
    }

    if(shouldUpdateState) {
        this->UpdateState();
    }
}

void Camera::UpdateState()
{
    const static Vector3 VAXIS(0.0f, 1.0f, 0.0f);

    Vector3 view(1.0f, 0.0f, 0.0f);
    view = glm::normalize(glm::rotate(view, glm::radians(mAngleH), VAXIS));

    Vector3 hAxis = glm::normalize(glm::cross(VAXIS, view));
    mTarget = glm::normalize(glm::rotate(view, glm::radians(mAngleV), hAxis));
    mUp = glm::normalize(glm::cross(mTarget, hAxis));

    this->ComputeViewMatrix();
}

} //end of namespace
