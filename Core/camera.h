#ifndef CAMERA_H
#define CAMERA_H

#include "TempoMath.h"
#include "InputEvent.h"

namespace TempoEngine
{
class Camera
{
public:
    Camera();
    Camera(const Vector3& position, const Vector3& target, const Vector3& up);

    void MakePerspectiveProjection(const float fov, const float aspectRation, const float zNear, const float zFar);
    void SetPosition(const Vector3& position);
    void SetTarget(const Vector3& target);
    void SetUp(const Vector3& up);
    const Vector3 GetPosition() const;
    const Vector3 GetTarget() const;
    const Vector3 GetUp() const;
    const Matrix4 GetViewProjectionMatrix() const;

    //Returns true if consumed the event, false otherwise

    bool OnInputEvent(const InputEvent& event);

    void Update();

private:
    Vector3 mPosition;
    Vector3 mTarget;
    Vector3 mUp;
    Matrix4 mViewMatrix;
    Matrix4 mProjectionMatrix;

    float mAngleH;
    float mAngleV;

    bool mOnUpperEdge;
    bool mOnLowerEdge;
    bool mOnLeftEdge;
    bool mOnRightEdge;

    glm::ivec2 mMousePos;

    void ComputeViewMatrix();
    void Init();
    void UpdateState();
    bool OnKeyboard(const InputEvent& event);
    void OnMouse(const InputEvent& event);

    static const float sStepSize;
};

inline const Vector3 Camera::GetPosition() const
{
    return mPosition;
}

inline const Vector3 Camera::GetTarget() const
{
    return mTarget;
}

inline const Vector3 Camera::GetUp() const
{
    return mUp;
}

inline const Matrix4 Camera::GetViewProjectionMatrix() const
{
    return mProjectionMatrix * mViewMatrix;
}

} //end of namespace
#endif // CAMERA_H
