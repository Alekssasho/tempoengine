#include "game.h"
#include "Platform.h"
#include "Renderer/HardwareBufferManager.h"
#include "Mesh.h"
#include "Renderer/texture.h"
#include "Renderer/GLRenderer.h"
#include "Scripting/ScriptStateManager.h"
#include "Scripting/ScriptExports.h"
#include "Scripting/ScriptResource.h"
#include "Resources/XMLResource.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <boost/range/adaptor/reversed.hpp>

#include <boost/filesystem/path.hpp>

namespace TempoEngine
{

GameOptions::GameOptions()
    : resourcePath(".")
    , screenSize(1024, 768)
    , fullscreen(false)
{
}

void GameOptions::Init(const Path& filePath)
{
    using namespace rapidxml;
    optionFile = filePath.String();
    file<> xmlFile(optionFile.c_str());
    xml_document<> doc;
    doc.parse<0>(xmlFile.data());

    auto root = doc.first_node();
    
    if(auto pNode = root->first_node("ScreenSize")) {
        if(auto width = pNode->first_attribute("width")) {
            if(auto height = pNode->first_attribute("height")) {
                this->screenSize = { std::stoi(width->value()), std::stoi(height->value()) };
            } else {
                LOG(WARNING) << "GameOptions: error in height";
            }
        } else {
            LOG(WARNING) << "GameOptions: error in weight";
        }
        if(auto fullscreen = pNode->first_attribute("fullscreen")) {
            this->fullscreen = fullscreen->value() == std::string("true") ? true : false;
        }
    } else {
        LOG(WARNING) << "GameOptions: error in ScreenSize";
    }

    if(auto pNode = root->first_node("ResourceFolder")) {
        if(auto attr = pNode->first_attribute("path")) {
            this->resourcePath = attr->value();
        } else {
            LOG(WARNING) << "GameOptions: error in Resource path";
        }
    } else {
        LOG(WARNING) << "GameOptions: error in ResourceFolder";
    }
}

Game* gGame = nullptr;

Game::Game()
    : mEventManager("mainEventManager", true)
{
    gGame = this;
    mIsRunning = false;
    mColorDepth = 32;
}

Game::~Game()
{
}

void Game::Destroy()
{
    mpGameLogic.reset();
    
    ScriptExports::Unregister();
    ScriptStateManager::DeleteInstance();
    
    mResManager.UnloadAll();

    HardwareBufferManager::Instance().Clear();
    SDL_GL_DeleteContext(mGLContext);
    SDL_DestroyWindow(mWindow);

    IMG_Quit();
    SDL_Quit();
}


    
bool Game::InitInstance()
{
    //Initialise options
    mOptions.Init(Path("PlayerOptions.xml"));
    
    mWindowedMode = !mOptions.fullscreen;

    //Initialise resource manager
    mResManager.Init(new DevelopmentResourceFile(mOptions.resourcePath));

    //Register default loaders
#define REGISTER_LOADER(loader) mResManager.RegisterLoader(std::unique_ptr<IResourceLoader>(new (loader)))
    
    REGISTER_LOADER(Renderer::MeshLoader);
    REGISTER_LOADER(Renderer::Texture2DLoader);
    REGISTER_LOADER(ScriptResourceLoader);
    REGISTER_LOADER(XMLResourceLoader);
    
#undef REGISTER_LOADER

    //Init SDL
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
        LOG(FATAL) << "SDL failed to init";

    //Setup for window and renderer initialisation
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    
    //Setup Scripting system
    ScriptStateManager::CreateInstance();
    
    if(!ScriptStateManager::Instance()->VInit()) {
        LOG(FATAL) << "ScriptStateManager failed to init";
    }
    
    ScriptExports::Register();
    
    mResManager.GetResource<ScriptResource>("PreInit.lua"); //Global PreInit file which handles main lua function

    //Create window
    mWindow = SDL_CreateWindow(this->VGetGameTitle().c_str(),
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               mOptions.screenSize.x, mOptions.screenSize.y,
                               SDL_WINDOW_OPENGL | (!mWindowedMode ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));

    SDL_GetWindowSize(mWindow, &mScreenSize.x, &mScreenSize.y);

    SDL_ShowCursor(SDL_DISABLE);

    //Create renderer context
    mGLContext = SDL_GL_CreateContext(mWindow);
    if(mGLContext == NULL)
        LOG(FATAL) << "There was an error creating OpenGL context";

    SDL_GL_MakeCurrent(mWindow, mGLContext);

    //Init SDL Image lib
    IMG_Init(0);

    //Initialise GLEW lib
    glewExperimental = GL_TRUE;
    GLenum glew_status = glewInit();
    if(glew_status != GLEW_OK)
        LOG(FATAL) << "Error init GLEW : " << glewGetErrorString(glew_status);
    glGetError(); // GlEW generates error 1280, this is to clean it
    
    mRenderer = std::make_shared<Renderer::GLRenderer>();

    mpGameLogic = this->VCreateGameAndView();

    mIsRunning = true;

    return true;
}

bool Game::VLoadGame()
{
    mpGameLogic->VLoadGame("");
    return false;
}

void Game::OnFrameRender()
{
    for(const auto& view : mpGameLogic->mGameViews) {
        view->VOnRender();
    }
}

void Game::OnUpdateFrame(unsigned long deltaTime)
{
    EventManager::Get()->Update(); //TODO: maybe put some time limit here

    mpGameLogic->VOnUpdate(deltaTime);
}

void Game::ProcessMessages()
{
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                mIsRunning = false;
                break;
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                InputEvent e;
                e.type = event.type == SDL_KEYDOWN ? InputEventType::KEYBOARD_DOWN_EVENT : InputEventType::KEYBOARD_UP_EVENT;
                e.key.code = event.key.keysym.sym;
                this->DispatchMessage(e);
                break;
            //TODO: add other events like mouse and joystick
            default:
                break;
        }
    }
}
    
void Game::DispatchMessage(InputEvent event)
{
    for(const auto& gameView : boost::adaptors::reverse(mpGameLogic->mGameViews)) {
        if(gameView->VOnMessage(event))
            break;//if returns true consume message
    }
}
    
void Game::StartMainLoop()
{
    long currTime = SDL_GetTicks();
    long lastTime = currTime;
    while(mIsRunning) {

        this->ProcessMessages();

        this->OnUpdateFrame(currTime - lastTime);
        this->OnFrameRender();

        SDL_GL_SwapWindow(mWindow);

        lastTime = currTime;
        currTime = SDL_GetTicks();
        VLOG(1) << "Frame time: " << currTime - lastTime << std::endl;
    }
}

int TempoEngineStart(int argc, char* argv[])
{
    google::InitGoogleLogging(argv[0]);
    //TODO: setup glog from file with options
    FLAGS_logtostderr = 1;
    FLAGS_v = 0;
    google::InstallFailureFunction([] { abort(); }); //TODO: make clear exit here
    
    boost::filesystem::path workingDir(argv[0]);
    Path::SetWorkingDir(workingDir.parent_path());
    
    if(!gGame->InitInstance()) {
        LOG(ERROR) << "Coudnt init game";
        return -1;
    }

    //Main loop

    gGame->StartMainLoop();

    gGame->Destroy();

    google::ShutdownGoogleLogging();

    return 0;
}

} //end of namespace
