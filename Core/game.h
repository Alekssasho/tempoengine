#ifndef GAME_H
#define GAME_H

#include <string>

#include "Interfaces.h"
#include "EventManager/EventManager.h"
#include "ResourceLoading.h"
#include "BaseGameLogic.h"

struct SDL_Window;

namespace TempoEngine
{

struct GameOptions
{
    std::string optionFile;

    //    std::string renderer;
    std::string resourcePath;
    iVector2 screenSize;
    bool fullscreen;

    //TODO: Add sound and volume options

    GameOptions();

    void Init(const Path& filePath);
};

class Game
{
protected:
    bool mWindowedMode;
    bool mIsRunning;
    Rect mDesktop;
    iVector2 mScreenSize;
    int mColorDepth;

    using BaseGameLogicPtr = std::unique_ptr<BaseGameLogic>;

    std::shared_ptr<Renderer::IRenderer> mRenderer;

    BaseGameLogicPtr mpGameLogic;

    EventManager mEventManager;

    ResourceManager mResManager;

    GameOptions mOptions;

    SDL_Window* mWindow;
    void* mGLContext;

    virtual void VRegisterGameEvents() {}

private:
    void RegisterEngineEvents();

    void OnFrameRender();
    void OnUpdateFrame(unsigned long deltaTime);
    void ProcessMessages();
    void DispatchMessage(InputEvent event);

public:
    Game();
    virtual ~Game();

    bool InitInstance();
    void Destroy();

    const iVector2& GetScreenSize() { return mScreenSize; }

    virtual std::string VGetGameTitle() = 0;
//    virtual std::string& VGetGameAppDirectory() = 0;
    //TODO: Icon

    const std::shared_ptr<Renderer::IRenderer>& GetRenderer() const { return mRenderer; }

    ResourceManager& GetResourceManager() { return mResManager; }

    virtual BaseGameLogicPtr VCreateGameAndView() = 0;
    virtual bool VLoadGame();

    void AbortGame() { mIsRunning = false; }
    bool IsRunning() const { return mIsRunning; }

    const BaseGameLogicPtr& GetGameLogic() const { return mpGameLogic; }

    void StartMainLoop();
};

extern Game* gGame;

extern int TempoEngineStart(int argc, char* argv[]);
} //end of namespace

#endif // GAME_H
