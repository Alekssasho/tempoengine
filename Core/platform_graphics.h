#ifndef PLATFORM_GRAPHICS_H
#define PLATFORM_GRAPHICS_H

#include <GL/glew.h>
#include <string>

#include "ResourceLoading.h"

namespace TempoEngine
{

const unsigned int INVALID_GRAPHICS_ID = 0xFFFFFFFF;

//TODO: Shaders effects must be loaded with Resource Loader
inline Path PathToEffect(const std::string& filename)
{
    return Path("../../Shaders/" + filename);
}

typedef GLuint GraphicsObjectID;
typedef GLenum GraphicsObjectEnum;

enum class BufferUsage {
    STATIC,
};

inline GLenum GetGLBufferUsage(BufferUsage usage)
{
    switch(usage) {
    case BufferUsage::STATIC:
        return GL_STATIC_DRAW;
    }
    return 0;
}

#define OPENGL_CHECK 1
#if OPENGL_CHECK == 1
#define TEMPO_GL_CHECK_ERROR(glFunc)                                                       \
    {                                                                                      \
        glFunc;                                                                            \
        int e = glGetError();                                                              \
        if(e != 0) {                                                                       \
            const char* errorString = "";                                                  \
            switch(e) {                                                                    \
            case GL_INVALID_ENUM:                                                          \
                errorString = "GL_INVALID_ENUM";                                           \
                break;                                                                     \
            case GL_INVALID_VALUE:                                                         \
                errorString = "GL_INVALID_VALUE";                                          \
                break;                                                                     \
            case GL_INVALID_OPERATION:                                                     \
                errorString = "GL_INVALID_OPERATION";                                      \
                break;                                                                     \
            case GL_INVALID_FRAMEBUFFER_OPERATION:                                         \
                errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";                          \
                break;                                                                     \
            case GL_OUT_OF_MEMORY:                                                         \
                errorString = "GL_OUT_OF_MEMORY";                                          \
                break;                                                                     \
            default:                                                                       \
                break;                                                                     \
            }                                                                              \
            LOG(FATAL) << "Error occured in " << #glFunc << " with error " << errorString; \
        }                                                                                  \
    }
#endif

} //end of namespace
#endif
