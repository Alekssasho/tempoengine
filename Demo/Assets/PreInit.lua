--Replace print and require function to use Engine equavilients
do
    local oldPrint = print;
    print = function(...)
        local res = "";
        for i, v in pairs({...}) do
            res = res .. tostring(v) .. "\t";
        end
        Engine.Log(res);
    end
end

do
    local oldRequire = require;
    require = function(script)
        if not Engine.ExecuteFile(script) then
            oldRequire(script)
        end
    end
end

-- Some utility functions
Utils = {}

function Utils.DumpTable(table)
    for key, val in pairs(table) do
        print(key, val);
    end
end

function Utils.TableLength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end


