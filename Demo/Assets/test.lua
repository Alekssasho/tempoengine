class 'DelayProcess' (Engine.Process)

function DelayProcess:__init(delay)
    Engine.Process.__init(self)
    self.delay = delay
end

function DelayProcess:OnUpdate(deltaMs)
    self.delay = self.delay - deltaMs
    if self.delay < 0 then
        self:Succeed()
    end
end

function DelayProcess:OnSuccess()
    print 'Working'
end

function Delegate(eventData)
    print(eventData.what)
end

--Engine.RegisterEventDelegate(TestEvent.eventType, Delegate)
Engine.CreateActor("Actors/test.xml")
Engine.CreateActor("Actors/sun.xml")