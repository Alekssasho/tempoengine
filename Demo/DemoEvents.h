//
//  DemoEvents.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 10/8/14.
//
//

#include <EventManager/EventManager.h>
#include <InputEvent.h>
#include <Scripting/ScriptEvent.h>

namespace Demo
{
    class EventTest : public TempoEngine::BaseEventData
    {
    public:
        Keycode keycode;
        static const TempoEngine::EventType kEventType;
        
        EventTest(Keycode code)
        : keycode(code)
        {}
        
        
        const TempoEngine::EventType& VGetEventType() const override { return kEventType; }
        const std::string VGetName() const override { return "EventTest"; }

    };

class ScriptEventTest : public TempoEngine::ScriptEvent
    {
    public:
        std::string what;
        
        const std::string& What() const { return what; }
        
        static const TempoEngine::EventType kEventType;
        
        ScriptEventTest(const std::string& w)
        : what(w)
        {}
        
        const TempoEngine::EventType& VGetEventType() const override { return kEventType; }
        const std::string VGetName() const override { return "ScriptEventTest"; }

        static luabind::scope RegisterScriptEvent()
        {
            using namespace luabind;
            return class_<ScriptEvent, ScriptEventTest>("TestEvent")
            .def(constructor<const std::string&>())
            .property("what", &ScriptEventTest::What)
            .enum_("constants")
            [
                value("eventType", kEventType)
            ];
        }
    };
}//end of namespace
