//
//  DemoGame.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 9/25/14.
//
//

#include "DemoGame.h"
#include "DemoHumanView.h"
#include "DemoEvents.h"
#include <Scripting/ScriptStateManager.h>
#include <Actors/CommonActorComponents.h>
#include <Renderer/Scene.h>

namespace Demo
{
    TempoEngine::Game::BaseGameLogicPtr DemoGame::VCreateGameAndView()
    {
        auto pGameLogic = new DemoGameLogic;
        pGameLogic->Init();
        
        pGameLogic->VAddView(std::make_unique<DemoHumanView>(this->mRenderer));
        
        return TempoEngine::Game::BaseGameLogicPtr(pGameLogic);
    }
    
    DemoGameLogic::DemoGameLogic()
    {
        TempoEngine::EventManager::Get()->AddListener(fastdelegate::MakeDelegate(this, &DemoGameLogic::EventTestDeleate), EventTest::kEventType);
//        TempoEngine::EventManager::Get()->AddListener(fastdelegate::MakeDelegate(this, &DemoGameLogic::ScriptEventTestDelegate), ScriptEventTest::kEventType);
        
        //Register Script Events
        luabind::module(TempoEngine::ScriptStateManager::Instance()->GetState())
        [
            ScriptEventTest::RegisterScriptEvent()
        ];
        
        
    }
    
    void DemoGameLogic::ScriptEventTestDelegate(TempoEngine::IEventDataPtr pEventData)
    {
        auto pScriptEvent = static_cast<ScriptEventTest*>(pEventData.get());
        LOG(INFO) << "Script:" << pScriptEvent->what;
    }
    
    void DemoGameLogic::EventTestDeleate(TempoEngine::IEventDataPtr pEventData)
    {
        TempoEngine::ScriptStateManager::Instance()->VExecuteFile("test.lua");
        
//        TempoEngine::EventManager::Get()->QueueEvent(IEventDataPtr( new ScriptEventTest("Lua is so coollllll")));
        
//        auto light = this->VCreateActor("Actors/sun.xml");
//        auto actor = this->VCreateActor("Actors/test.xml");
//        auto pHumanView = static_cast<HumanView*>(mGameViews.front().get());
//        pHumanView->mpScene->GetScene().GetCamera()->GetFrustum().SetFov(Degree(60));
//        pHumanView->mpScene->GetScene().GetCamera()->SetCameraOffset({13.0f, 17.0f, 10.0f});
//        pHumanView->mpScene->GetScene().GetCamera()->SetTarget(std::static_pointer_cast<TempoEngine::Renderer::SceneNode>(pHumanView->mpScene->GetScene().FindActor(actor->GetId())));
    }
    
    void DelayProcess::VOnUpdate(U32 deltaMs)
    {
        mDelaySoFar += deltaMs;
        if(mDelaySoFar >= mDelay)
            this->Succeed();
    }
    
    void DelayProcess::VOnSuccess()
    {
        LOG(INFO) << "OnSuccess on DelayProcess";
    }
}