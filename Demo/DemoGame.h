//
//  DemoGame.h
//  Engine
//
//  Created by Aleksandar Angelov on 9/25/14.
//
//

#ifndef __Engine__DemoGame__
#define __Engine__DemoGame__

#include <game.h>
#include <Process.h>

namespace Demo
{
using namespace TempoEngine;
    
class DemoGame : public TempoEngine::Game
{
public:
    std::string VGetGameTitle() override { return "Demo Game"; }
    BaseGameLogicPtr VCreateGameAndView() override;
};

class DemoGameLogic : public TempoEngine::BaseGameLogic
{
public:
    DemoGameLogic();
    void EventTestDeleate(TempoEngine::IEventDataPtr pEventData);
    void ScriptEventTestDelegate(TempoEngine::IEventDataPtr pEventData);
};

class DelayProcess : public TempoEngine::Process
{
public:
    explicit DelayProcess(U32 delay)
    :mDelay(delay)
    {}
protected:
    void VOnUpdate(U32 deltaMs) override;
    void VOnSuccess() override;
private:
    U32 mDelay;
    U32 mDelaySoFar = 0;
};
}

#endif /* defined(__Engine__DemoGame__) */
