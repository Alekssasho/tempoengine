//
//  DemoHumanView.cpp
//  Engine
//
//  Created by Aleksandar Angelov on 9/26/14.
//
//

#include "DemoHumanView.h"
#include <Mesh.h>
#include <TempoMath.h>
#include <game.h>
#include "DemoEvents.h"

namespace Demo
{
    
    const TempoEngine::EventType EventTest::kEventType(0x00000000);
    const TempoEngine::EventType ScriptEventTest::kEventType(0x00000001);
    
    bool DemoHumanView::VOnCreate()
    {
        using namespace TempoEngine;
        using namespace TempoEngine::Renderer;
        
        mKeyboardHandler.reset(new DemoController);
        
        gGame->GetRenderer()->VSetBackgroundColor({ 0.0f, 0.0f, 0.4f, 1.0f });

        return HumanView::VOnCreate();
    }
    
    
    bool DemoController::VOnPointerMove(const Vector2& pos, const int radius)
    {
        return false;
    }
    
    bool DemoController::VOnPointerButtonDown(const Vector2& pos, const int radius, const std::string& buttonName)
    {
        return false;
    }
    
    bool DemoController::VOnPointerButtonUp(const Vector2& pos, const int radius, const std::string& buttonName)
    {
        return false;
    }
    
    bool DemoController::VOnKeyDown(const Keycode c)
    {
        return true;
    }
    
    bool DemoController::VOnKeyUp(const Keycode c)
    {
        if(c == SDLK_ESCAPE) {
            TempoEngine::gGame->AbortGame();
        } else {
            TempoEngine::EventManager::Get()->QueueEvent(std::make_shared<EventTest>(c));
        }
        return true;
    }
}