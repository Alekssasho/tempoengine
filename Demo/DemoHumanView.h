//
//  DemoHumanView.h
//  Engine
//
//  Created by Aleksandar Angelov on 9/26/14.
//
//

#ifndef __Engine__DemoHumanView__
#define __Engine__DemoHumanView__

#include <HumanView.h>

namespace Demo
{
    class DemoHumanView : public TempoEngine::HumanView
    {
    public:
        DemoHumanView(std::shared_ptr<TempoEngine::Renderer::IRenderer> pRenderer)
        : HumanView(pRenderer)
        {
        }
        
        bool VOnCreate() override;
    };
    
    using TempoEngine::Vector2;
    using TempoEngine::Keycode;
    
    class DemoController : public TempoEngine::IKeyboardHandler, public TempoEngine::IPointerHandler
    {
        bool VOnPointerMove(const Vector2& pos, const int radius) override;
        bool VOnPointerButtonDown(const Vector2& pos, const int radius, const std::string& buttonName) override;
        bool VOnPointerButtonUp(const Vector2& pos, const int radius, const std::string& buttonName) override;
        bool VOnKeyDown(const Keycode c) override;
        bool VOnKeyUp(const Keycode c) override;
    };
}

#endif /* defined(__Engine__DemoHumanView__) */
