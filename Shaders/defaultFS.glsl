#version 330 core
in vec2 texCoord;
out vec4 color;

uniform sampler2D mTextureSampler;

void main()
{
	color = texture(mTextureSampler, texCoord);
}
