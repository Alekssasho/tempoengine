#version 330 core

#extension GL_ARB_explicit_uniform_location : enable

#ifdef GL_ARB_explicit_uniform_location
#define UseLocation(x) layout(location = (x))
#else
#define UseLocation(x)
#endif

in vec2 fragTexCoord;
in vec3 fragNormal;
in vec3 fragWorldPos;

out vec4 fragColor;

struct BaseLight
{
	vec3 color;
	float ambientIntensity;
	float diffuseIntensity;
};

struct DirectionalLight
{
	BaseLight base;
	vec3 direction;
};

struct Attenuation
{
	float constant;
	float linear;
	float exp;
};

struct PointLight
{
	BaseLight base;
	vec3 position;
	Attenuation att;
};

struct SpotLight
{
	PointLight base;
	vec3 direction;
	float cutoff;
};

const int MAX_POINT_LIGHTS = 2;
const int MAX_SPOT_LIGHTS = 2;

uniform int gNumPointLights;
uniform int gNumSpotLights;
UseLocation(10) uniform DirectionalLight gDirLight;
UseLocation(20) uniform PointLight gPointLights[MAX_POINT_LIGHTS];
UseLocation(40) uniform SpotLight gSpotLights[MAX_SPOT_LIGHTS];
uniform vec3 gEyeWorldPos;
uniform float gMatSpecularIntensity;
uniform float gSpecularPower;
uniform sampler2D mTextureSampler;

vec4 CalcLightInternal(BaseLight light, vec3 lightDir, vec3 normal)
{
	vec4 ambientColor = vec4(light.color, 1.0f) * light.ambientIntensity;
	float diffuseFactor = dot(normal, -lightDir);
	
	vec4 specularColor = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	vec4 diffuseColor = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	
	if(diffuseFactor > 0) {
	
		vec3 vertexToEye = normalize(gEyeWorldPos - fragWorldPos);
		vec3 lightReflect = normalize(reflect(lightDir, normal));
		float specularFactor = dot(vertexToEye, lightReflect);
		specularFactor = pow(specularFactor, gSpecularPower);
		
		specularColor = vec4(light.color, 1.0f) * gMatSpecularIntensity * max(specularFactor, 0.0f);
		
		diffuseColor = vec4(light.color, 1.0f)  * light.diffuseIntensity * diffuseFactor;
	}
	
	return (ambientColor + specularColor + diffuseColor);
}

vec4 CalcDirectionalLight(vec3 normal)
{
	return CalcLightInternal(gDirLight.base, gDirLight.direction, normal);
}

vec4 CalcPointLight(PointLight light, vec3 normal)
{
	vec3 lightDirection = fragWorldPos - light.position;
	float dist = length(lightDirection);
	lightDirection = normalize(lightDirection);
	
	vec4 color = CalcLightInternal(light.base, lightDirection, normal);
	float atten = light.att.constant +
					light.att.linear * dist +
					light.att.exp * dist * dist;
						
	return color / atten;
}

vec4 CalcSpotLight(SpotLight light, vec3 normal)
{
	vec3 lightToPixel = normalize(fragWorldPos - light.base.position);
	float spotFactor = dot(lightToPixel, light.direction);

	if(spotFactor > light.cutoff) {
		vec4 color = CalcPointLight(light.base, normal);
		return color * (1.0f - (1.0f - spotFactor) * 1.0f/(1.0f - light.cutoff));
	} else {
		return vec4(0.0f, 0.0f, 0.0f, 0.0f);
	}
}

void main()
{
	vec3 normal = normalize(fragNormal);
	vec4 totalLight = CalcDirectionalLight(normal);
	
	for(int i = 0; i < gNumPointLights; ++i) {
		totalLight += CalcPointLight(gPointLights[i], normal);
	}

	for(int i = 0; i < gNumSpotLights; ++i) {
		totalLight += CalcSpotLight(gSpotLights[i], normal);
	}
	
	fragColor = texture(mTextureSampler, fragTexCoord) * totalLight;
}
