#version 330 core
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;

uniform mat4 MVP;
uniform mat4 gWorld;

out vec2 fragTexCoord;
out vec3 fragNormal;
out vec3 fragWorldPos;

void main()
{
	fragNormal = (gWorld * vec4(vertexNormal, 0.0f)).xyz;
	fragWorldPos = (gWorld * vec4(vertexPosition, 1.0f)).xyz;
	fragTexCoord = vertexUV;
	gl_Position = MVP * vec4(vertexPosition, 1.0f);
}
